<?php
require_once('config.php');
require_once('classes.php');
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION['access'])){
  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_level();
if($access->access_level != 1 AND $access->access_level != 2) {
  header("Location: index.php");
}
function nysensor(){
  $epost = $_GET['epost'];
  $navn = $_GET['navn'];
  $fornavn = $_GET['fornavn'];
  global $access;

  $sql = "SELECT id FROM ekstern_sensor WHERE email = ?";
  $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
  if (!$con->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $con->error);
  }
  $stmt = $con->prepare($sql);
  $stmt->bind_param("s", $epost);
  $stmt->execute();
  if($stmt->num_rows > 0){
    $stmt->bind_result($id);
    $stmt->fetch();
    return(json_encode($id));
  }else{
    $stmt->close();
    $sql = "INSERT INTO ekstern_sensor (email, navn, fornavn) VALUES (?, ?, ?)";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("sss", $epost, $navn, $fornavn);
    $stmt->execute();
    $id = $stmt->insert_id;
    $stmt->close();
    $sql = "SELECT * FROM ekstern_sensor WHERE id = ?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()){
	    $sensor[] = $row;
    }
    $log = new log;
    $log->sensor = $id;
    $log->endring = "Ny sensor opprettet";
    $log->log_sensor($access->current_user);

    return($sensor);
  }
  $con->close();
}

function vissensor($id){
  $sql = "SELECT id FROM ekstern_sensor WHERE email = ?";
  $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
  if (!$con->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $con->error);
  }
  $stmt = $con->prepare($sql);
  $stmt->bind_param("s", $epost);
  $stmt->execute();
}

if(isset($_GET['nysensor']) AND $_GET['nysensor'] == "true"){
  echo json_encode(nysensor());
}
