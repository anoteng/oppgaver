<?php
// Handling data in JSON format on the server-side using PHP
//
include('config.php');
include('classes.php');
header("Content-Type: application/json");
// build a PHP variable from JSON sent using POST method
$v = json_decode(stripslashes(file_get_contents("php://input")));
// encode the PHP variable to JSON and send it back on client-side
//echo json_encode($v);
//var_dump($v);
$con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
if (!$con->set_charset("utf8")) {
  printf("Error loading character set utf8: %s\n", $con->error);
}

foreach($v as $object){
  $sql = "SELECT termin.id FROM termin WHERE termin.year = ? AND termin.month = ?";
  //var_dump($object);
  $stmt = $con->prepare($sql);
  $stmt->bind_param("ss", $object->arstall, $object->vurdtidkode);
  $stmt->execute();
  $result = $stmt->get_result();
  if($result->num_rows != 1){
    echo "Oppretter ny termin: " . $object->arstall . "-" . $object->vurdtidkode . "\r\n";
    $sql = "INSERT INTO termin (year, month) VALUES (?, ?)";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("ss",$object->arstall, $object->vurdtidkode);
    $stmt->execute();
    $sql = "SELECT termin.id FROM termin WHERE termin.year = ? AND termin.month = ?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("ss", $object->arstall, $object->vurdtidkode);
    $stmt->execute();
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()){
      $termid = $row['id'];
    }
  }else{
    while ($row = $result->fetch_assoc()){
      $termid = $row['id'];
    }
  }

  // Sjekk om student eksisterer

  $sql = "SELECT * FROM studenter WHERE id = ?";
  $stmt = $con->prepare($sql);
  $stmt->bind_param("i", $object->studentnr_tildelt);
  $navn = $object->etternavn . ", " . $object->fornavn;
  $stmt->execute();
  $result = $stmt->get_result();
  if($result->num_rows != 1){
    echo "Oppretter ny student: " . $object->etternavn . ", " . $object->fornavn . " med epost " . $object->emailadresse  . "\r\n";
    $sql = "INSERT INTO studenter (id, navn, epost) VALUES (?, ?, ?)";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("iss", $object->studentnr_tildelt, $navn, $object->emailadresse);
    $stmt->execute();
    $sql = "SELECT * FROM studenter WHERE navn = ? OR epost = ?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("ss", $navn, $object->emailadresse);
    $stmt->execute();
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()){
      $studentid = $row['id'];
    }
  }else{
    while ($row = $result->fetch_assoc()){
      $studentid = $row['id'];
    }
  }

  // Sjekk om emnekode eksisterer

  $sql = "SELECT * FROM emner WHERE emnekode = ?";
  $stmt = $con->prepare($sql);
  $stmt->bind_param("s", $object->emnekode);
  $stmt->execute();
  $result = $stmt->get_result();
  if($result->num_rows != 1){
    echo "Oppretter ny emnekode: " . $object->emnekode . "\r\n";
    $sql = "INSERT INTO emner (emnekode) VALUES (?)";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("s", $object->emnekode);
    $stmt->execute();
    $sql = "SELECT * FROM emner WHERE emnekode = ?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("s", $object->emnekode);
    $stmt->execute();
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()){
      $emnekode = $row['emnekode'];
    }
  }else{
    while ($row = $result->fetch_assoc()){
      $emnekode = $row['emnekode'];
    }
  }

  // Sjekk om vurderingsenhet eksisterer

  $sql = "SELECT * FROM vurderingsenhet WHERE emne = ? AND termin = ?";
  $stmt = $con->prepare($sql);
  $stmt->bind_param("si", $emnekode, $termid);
  $stmt->execute();
  $result = $stmt->get_result();
  if($result->num_rows != 1){
    echo "Oppretter ny vurderingsenhet: " . $emnekode . " " . $object->arstall . "-" . $object->vurdtidkode . "\r\n";
    $sql = "INSERT INTO vurderingsenhet (emne, termin) VALUES (?, ?)";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("si", $emnekode, $termid);
    $stmt->execute();
    $sql = "SELECT * FROM vurderingsenhet WHERE emne = ? AND termin = ?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("si", $emnekode, $termid);
    $stmt->execute();
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()){
      $vurderingsenhet = $row['id'];
    }
  }else{
    while ($row = $result->fetch_assoc()){
      $vurderingsenhet = $row['id'];
    }
  }

  // Sjekk om vurderingsmelding eksisterer

  $sql = "SELECT * FROM vurderingsmelding WHERE studentid = ? AND vurderingsenhet = ?";
  $stmt = $con->prepare($sql);
  $stmt->bind_param("ii", $studentid, $vurderingsenhet);
  $stmt->execute();
  $result = $stmt->get_result();
  if($result->num_rows != 1) {
    echo "Oppretter ny vurderingsmelding: " . $emnekode . " " . $object->arstall . "-" . $object->vurdtidkode . " " . $object->etternavn . ", " . $object->fornavn . "\r\n";
    $sql = "INSERT INTO vurderingsmelding (studentid, vurderingsenhet) VALUES (?, ?)";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("ii", $studentid, $vurderingsenhet);
    $stmt->execute();
  }else{
    echo "Vurderingsmelding finnes allerede: " . $emnekode . " " . $object->arstall . "-" . $object->vurdtidkode . " " . $object->etternavn . ", " . $object->fornavn . "\r\n";
  }
}
$con->close();
