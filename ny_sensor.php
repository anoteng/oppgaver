<?php
include("config.php");
include("classes.php");
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION['access'])){
  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_level();
if($access->access_level != 1){
  header("Location: index.php");
}
$vurderingsenhet = new vurderingsenhet;
$vurderingsenhet->list_eksterne();

?>
<script src="js/csv.js"></script>
<script src="js/main.js"></script>

<div class="container-fluid">
<!--  <script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <link rel="stylesheet" href="css/csv.css">
  <div class="file-upload">
    <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button>

    <div class="image-upload-wrap">
      <input class="file-upload-input" type='file' onchange="readURL(this);" accept="text/csv" />
      <div class="drag-text">
        <h4>Dra og slipp en CSV-fil med nye sensorer her</h4>
        <p>fila må være i formatet<br />
        <code type="">email;fornavn;etternavn</code><br />
        uten kolonneheading</p>
      </div>
    </div>
    <div class="file-upload-content">
      <img class="file-upload-image" src="#" alt="sensorlise" />
      <div class="image-title-wrap">
        <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
      </div>
    </div>
  </div>-->

  <table id="sensorliste" class="table table-hover">
    <tr>
      <th>ID</th>
      <th>epost</th>
      <th>etternavn</th>
      <th>fornavn</th>
      <th>Slett</th>
    </tr>
    <form id="nysensor" name="nysensor" action="ny_sensor_ajax.php?ny=true" method="post" data-autosubmit>
      <tr>
        <th scope='row'>genereres </th>
        <td>
          <input type="text" name="email" id='ny-email'/>
        </td>
        <td>
          <input type="text" name="navn" id='ny-navn'/>
        </td>
        <td>
          <input type="text" name="fornavn" id='ny-fornavn'/>
        </td>
        <td>
          <button type="button" onclick="nySensor(document.getElementById('sensorliste'))">Lagre</button>
        </td>
      </tr>
    </form>

    <?php
    foreach($vurderingsenhet->ekstern_sensor as $sensor){
      echo "\t\t<tr>\r\n";
        echo "\t\t\t<th scope='row'>\r\n";
          echo "{$sensor['id']}";
        echo "\t\t\t</th>\r\n";
      echo "\t\t\t<td>\r\n";
      echo "\t\t\t\t{$sensor['email']}\r\n";
      echo "\t\t\t</td>\r\n";
      echo "\t\t\t<td>\r\n";
      echo "\t\t\t\t{$sensor['navn']}\r\n";
      echo "\t\t\t</td>\r\n";
      echo "\t\t\t<td>\r\n";
      echo "\t\t\t\t{$sensor['fornavn']}\r\n";
      echo "\t\t\t</td>\r\n";
      echo "\t\t\t<td>\r\n";
      echo "\t\t\t\t<a href=\"#\" onchange=\"vissensor({$sensor['id']})\">Rediger</a>";
      echo "\t\t\t</td>\r\n";
      echo "\t\t</tr>\r\n";
    }
    ?>
  </table>
</div>
