<?php
ini_set('session.gc_maxlifetime', 24*60*60);
ini_set('session.cookie_lifetime', 24*60*60);
include('config.php');
include('classes.php');
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION['access'])){
  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_token();
$access->check_access_level();
//$access->hent_terminer();
//var_dump($access->access_level);

?>
<!DOCTYPE html>
<html lang="no">

<head>
  <meta charset="utf-8">
  <title>OppgaveWeb</title>

  <link rel="manifest" href="site.webmanifest">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="css/main.css">
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <!-- Popper JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <script src="js/klagesensur.js"></script>
  <script src="js/emneadministrasjon.js"></script>
  <script src="js/fagpersonadm.js"></script>
  <script src="js/main.js?v1" async></script>

  <!-- Select2 -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

</head>

<body>

  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->
  <div id="spinner"><img src="img/Curve-Loading.gif"></div>
  <div class="container-fluid pt-3">
    <div class="row">
      <div class="col-2">
        <div id="logo"><a href=""><img src="img/ibm.jpg" width = "60%" align="left"></a></div>
      </div>
      <div class="col-1" id="semestervelgerDiv">
        <label>Semestervelger: <select class="nav-link" id="termselector" onchange="skift_termin()">
            <?php
            echo $access->hent_terminer();
            ?>
          </select></label>
      </div>
      <div class="col-1">
        <a href="https://studntnu.sharepoint.com/sites/TeamSite/2906/_layouts/15/WopiFrame.aspx?sourcedoc={1031454c-f6c3-4db8-be02-5e2bf083f039}&action=edit&wd=target%28%2F%2FStartside%20-%20Landing%20page.one%7Cc7f4920f-e57b-4ece-8962-07985e4187d3%2FIBM%20Administrasjonsguide%7Cc13dff82-908d-4612-ae29-1a44f2f109aa%2F%29" target="_blank">Administrasjonsguiden</a>
      </div>
      <div class="col"></div>
    </div>

<a href="#meny" data-toggle="collapse">vis/lukk meny</a>
    <div class="row">
      <div class="col-sm-2 collapse show" id="meny">
	<ul class="nav flex-column">
	  <li class="nav-item">
      <a class="nav-link" href="#" onclick="$('#ajax-content').load('veileder.php')">Vis alle emner</a>
    </li>
	  <li class="nav-item">
	    <a class="nav-link" href="#" onclick="$('#ajax-content').load('veileder.php?faggruppe=true')">Vis egen faggruppe</a>
    </li>
	  <li class="nav-item">
	    <a class="nav-link" href="#" onclick="$('#ajax-content').load('rapporter.php')">Vis mine studenter</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="https://hjelp.ntnu.no/tas/public/ssp/content/serviceflow?unid=bdfe20c8fa764d6099cd889e893daa78" target="_blank">Legg til ekstern sensor</a>
    </li>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#" id="klagesensurLink">Klagesensur</a>
    </li>

    <?php
    if($access->access_level == 1){
      ?>
      <li class="nav-item">
        <a class="nav-link" href="#" onclick="$('#ajax-content').load('ny_sensor.php')">ADM: Rediger eksterne sensorer</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" onclick="$('#ajax-content').load('import.php')">ADM: Importer nye vurderingsmeldinger</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" id="emneadministrasjon">ADM: Emneadministrasjon</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" id="fagpersonadministrasjon">ADM: Fagpersonadministrasjon</a>
      </li>
    <?php
    }

    ?>
	</ul>
      </div>
      <div class="col-sm" id="ajax-content">
          <?php
          switch($access->access_level){
            case 1:
              include('veileder.php');
	      break;
            case 2:
		    include('veileder.php');
		    break;
            case 3:
		    include('sensor.php');
		    break;
          }
          ?>
      </div>

    </div>
  </div>
  <!-- Footer -->
  <footer class="footer">
    <!-- Copyright -->
    <div class="container">
      <span class="text-muted">
        <a href="https://gitlab.com/anoteng/oppgaver/-/issues">Report bugs and feature requests here</a><br>
        v. 0.9.5 - <a href="changelog.php">Changelog</a> - © 2020-2022 Andreas Noteng -
        <a href="https://www.gnu.org/licenses/gpl-3.0.html">Software licenced under the GPL v. 3</a><br>
        <a href="copyright">Full copyright information</a>
      </span>
    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->
<script>
  function skift_termin() {
    var selector = document.getElementById('termselector');
    var value = selector.value;
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', "session.php?termin=".concat(value));
    httpRequest.send(null);
  }
</script>
</body>

</html>
