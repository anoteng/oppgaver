<?php
include("config.php");
include("classes.php");
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION['access'])){
  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_level();
if($access->access_level != 1 AND $access->access_level != 2) {
  header("Location: index.php");
}
if(isset($_GET['nyoppgave'])){
  $sql = "insert into oppgave (emnekode, veileder, sensor1, sensor2, oppgavenr, tittel, tilknyttet_emne, bakgrunn, beskrivelse, antall_stud, annen_kontakt, eksterne, hovedprofil, master, prosjekt) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
  $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
  if (!$con->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $con->error);
  }
  $stmt = $con->prepare($sql);
  $stmt->bind_param("siiisssssisssii", $_POST['emnekode'], $_POST['veileder'], $_POST['sensor1'], $_POST['sensor2'], $_POST['oppgavenr'], $_POST['tittel'], $_POST['tilknyttet_emne'], $_POST['bakgrunn'], $_POST['beskrivelse'], $_POST['antall_stud'], $_POST['annen_kontakt'], $_POST['eksterne'], $_POST['hovedprofil'], $_POST['master'], $_POST['prosjekt']);
  $stmt->execute();

//  echo $stmt->error ."<br />";
  echo $con->insert_id;
//  var_dump($_POST);
}
if(isset($_GET['visoppgave'])){
  if($_GET['visoppgave'] == "alle"){
    $sql = "SELECT oppgave.id, oppgave.emnekode, oppgave.veileder, oppgave.tittel, brukere.fornavn, brukere.navn FROM oppgave INNER JOIN brukere ON oppgave.veileder=brukere.id";
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = array();
    $i = 0;
    while($row = $result->fetch_assoc()){
      $data[$i] = $row;
      $i++;
    }
    echo json_encode($data);
  }
}
if(isset($_GET['getveileder'])){
  $sql = "SELECT brukere.* FROM brukere INNER JOIN tilganger ON brukere.id=tilganger.brukerid WHER tilganger.rolleid = 2";
  $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
  if (!$con->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $con->error);
  }
  $stmt = $con->prepare($sql);
  $stmt->execute();
  $result = $stmt->get_result();
  $data = array();

  while($row = $result->fetch_assoc()){
    $data[] = $row;
  }
  echo json_encode($data);
}
