<?php
include("config.php");
include("classes.php");
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION['access'])){
  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_level();
if($access->access_level != 1 AND $access->access_level != 2){
  header("Location: index.php");
}
$student = new student;
if(isset($_GET['studentinfo']) and !isset($_GET['vurdmelding'])){
  $student->studentid = $_GET['studentinfo'];
  $student->listStudMeldinger();
  echo(json_encode($student->studentinfo));
}elseif(isset($_GET['studentinfo']) and isset($_GET['vurdmelding'])){
  $student->studentid = $_GET['studentinfo'];
  $student->listStudMeldinger($_GET['vurdmelding']);
  echo(json_encode($student->studentinfo));
}
