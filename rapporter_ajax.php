<?php
include("config.php");
include("classes.php");
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION['access'])){
  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_level();
if($access->access_level != 1 AND $access->access_level != 2){
  header("Location: index.php");
}

$veilederrapport = new rapporter;
if(isset($_GET['veileder'])){
  $veilederrapport->veilederoppgaver($_GET['veileder'], $_GET['termin']);
//  echo(json_encode(array_values($veilederrapport->studentliste)));
}
if(isset($_GET['sensor'])){
  $veilederrapport->sensoroppgaver($_GET['sensor'], $_GET['termin']);
//  echo(json_encode(array_values($veilederrapport->studentliste)));
}
