<?php
//if(!isset($_SESSION['access'])){
//header("Location: login.php");
//}
//$access = $_SESSION['access'];
//$access->check_access_token();
//$access->check_access_level();
?>
<div class="filters container-fluid">
  <h3>Filtrer emner etter:</h3>
  <div class="row">
    <div class="col-md-3 pr-md-3">
      <label for="faggruppeSelector">Faggruppe</label>
    </div>
    <div class="col-md-3 pr-md-3">
      <label for="emneansvarligSelector">Emneansvarlig:</label>
    </div>
    <div class="col-md-6 pr-md-6">
      <label for="emnekodeSelector">Emnekode:</label>
    </div>
  </div>
  <div class="row no-gutters">
    <div class="col-md-3 pr-md-3">
      <select id="faggruppeSelector" name="faggruppeSelector">
        <option value="none" selected></option>
      </select>
    </div>
    <div class="col-md-3 pr-md-3">
      <select id="emneansvarligSelector" class="js-example-basic-single" name="emneansvarligSelector">
        <option value="none" selected></option>
      </select>
    </div>
    <div class="col-5">
      <select id="emnekodeSelector" class="js-example-basic-single" name="emnekodeSelector">
        <option value="none" selected></option>
      </select>
    </div>
    <div class="col"><button id="nullstill">Nullstill filter</button></div>
  </div>
</div>

<div class="container-fluid">
  <div class="row overskrifterSensorliste">
    <div class="col-md-1 pr-md-1 sensorColEmnekode" onclick="sensorliste.orderNodes()" style="cursor: pointer" title="Klikk for å sortere etter emnekode"><b>Emnekode</b></div>
    <div class="col-md-3 pr-md-3 sensorColEmneansvarlig"><b>Emneansvarlig</b></div>
    <div class="col-md-4 pr-md-4 sensorColIntern"><b>Interne sensorer</b></div>
    <div class="col-md-4 pr-md-4 sensorColEkstern"><b>Eksterne sensorer</b></div>
  </div>
  <div id="sensorRowsPlaceholder"></div>
</div>
<div class="row no-gutters sensorRowTemplate sensoremne" id="sensorRowTemplate">
  <div class="col-md-1 pr-md-1 sensorColEmnekode"></div>
  <div class="col-md-3 pr-md-3 sensorColEmneansvarlig"></div>
  <div class="col-md-4 pr-md-4 sensorColIntern">
    <ol class="sensorliste"></ol>
  </div>
  <div class="col-md-4 pr-md-4 sensorColEkstern">
    <ol class="sensorliste"></ol>
  </div>
<!--  <div class="col-md-1 pr-md-1 sensorColEdit"></div>-->
</div>
<!--<button type="button" class="btn btn-default btn-sm hidden" id="editButtonTemplate">-->
<!--  <span><img src="img/84380.png" width="20px"></span>-->
<!--</button>-->
<!--<div id="overlay"><div id="overlayText">Placeholder text</div></div>-->
