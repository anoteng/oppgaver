<?php
require_once('config.php');
require_once('classes.php');
if(!isset($_SESSION['access'])){
	  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_token();
$access->check_access_level();
$oppgaver = new oppgave();
if(isset($_GET['faggruppe'])){
  $oppgaver->list_oppgaver($access->faggruppe);
}else {
  $oppgaver->list_oppgaver();
}
//var_dump($oppgaver->oppgaveliste);
//echo $access->hent_terminer();
?>
<div class="container filters">
  <div class="row no-gutters heading">
    <div class="col-md-1 pr-md-1">Emnekode</div>
    <div class="col-md-4 pr-md-1">Emnenavn</div>
    <div class="col-md-2 pr-md-1">Emneansvarlig</div>
    <div class="col-md-1 pr-md-1">Faggruppe</div>
    <div class="col-md-2 pr-md-1">Antall komplett</div>
    <div class="col-md-2 pr-md-1">Veileder/sensor1/sensor2</div>
  </div>
  <?php
  foreach($oppgaver->oppgaveliste as $row) {
    ?>
  <div class="row rowhover">
    <div class="col-md-1"><a href="#" onclick="$('#ajax-content').load('<?php echo SCRIPT_URL . "/emne.php?id=" . $row['emne'] . "&amp;terminid=" . $oppgaver->terminid . "&amp;vurdid=" . $row['vurdid'] ?>');return false;"><?php echo $row['emne'] ?></a></div>

      <!-- <td><a href="emne.php?id=<?php echo $row['emne'] ?>&terminid=<?php echo $oppgaver->terminid ?>&vurdid=<?php echo $row['vurdid'] ?>" onclick="lastEmne(<?php echo $row['emne'] .', '. $oppgaver->terminid .', '. $row['vurdid'] ?>)"><?php echo $row['emne'] ?></a></td> -->
    <div class="col-md-4"><?php echo $row['emnenavn'] ?></div>
    <div class="col-md-2"><?php echo $row['emneansvarlig'] ?></div>
    <div class="col-md-1"><?php echo $row['fag_kort'] ?></div>
    <div class="col-md-2">
    <?php
    $vurderingsenhet = new vurderingsenhet;
    $vurderingsenhet->emnekode = $row['emnenavn'];
    $vurderingsenhet->termin = $oppgaver->terminid;
    $vurderingsenhet->id = $row['vurdid'];
    $vurderingsenhet->count_locked();
    echo "$vurderingsenhet->count_locked / $vurderingsenhet->count";
    ?>
    </div>
    <div class="col-md-2">
      <?php
      echo "$vurderingsenhet->count_veileder / $vurderingsenhet->count_sensor1 / $vurderingsenhet->count_sensor2";
      ?>
    </div>
  </div>
    <?php
  }
  ?>
</div>
