<?php
include("config.php");
include("classes.php");
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION['access'])){
  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_level();
if($access->access_level != 1 AND $access->access_level != 2) {
  header("Location: index.php");
}

$emner = array();
$brukere = new brukere();
$brukere->finn_brukere();

function list_emner(){
  global $emner;
  $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
  $sql = "SELECT emner.*  FROM emner";
  $stmt = $con->prepare($sql);
  $stmt->execute();
  $result = $stmt->get_result();
  while($row = $result->fetch_assoc()){
    //var_dump($row);
    $emner[] = $row;
  }
  //var_dump($this->vurderingsenheter);
  $stmt->close();
}

function vis_oppgave($id){
  $sql = "SELECT oppgave.id, oppgave.emnekode, oppgave.veileder, oppgave.tittel, brukere.fornavn, brukere.navn FROM oppgave INNER JOIN brukere ON oppgave.veileder=brukere.id WHERE oppgave.id = ?";
  $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
  if (!$con->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $con->error);
  }
  $stmt = $con->prepare($sql);
  $stmt->bind_param("i", $id);
  $stmt->execute();
  $result = $stmt->get_result();
  $data = array();
  $row = $result->fetch_assoc();
  $felter = array("emnekode", "veileder", "sensor1", "sensor2", "oppgavenr", "tittel", "tilknyttet_emne", "bakgrunn", "beskrivelse", "antall_stud", "annen_kontakt", "eksterne", "hovedprofil", "bilde", "master", "prosjekt", "aktiv");
  foreach($felter as $felt){
    echo "<strong>$felt</strong><br/>";
    if(isset($row[$felt])){
      echo "<p id=\"$felt\" class=\"felt\">$row[$felt]</p>";
    }else{
      echo "<p id=\"$felt\" class=\"felt\">&hellip;</p>";
    }
  }
  echo "<a id=\"tilbake\">Tilbake</a>";
  ?>
  <script>
    function lagListe(id){
      var liste;
      $.post( "oppgave_ajax_query.php?get".concat(id.concat('=true')), function(data){
          console.log(data);
        });
    }

    $(document).on('click', '.felt', function(e){
      var id = e.currentTarget.id;
      var value = document.getElementById(id);
      var newdiv = `<div id="tempdiv" class="editbox">`;
      if (id === "emnekode"){
        newdiv = newdiv.concat(`<select name="${id}" id="${id}">`);
        newdiv = newdiv.concat(lagListe(id));
        newdiv = newdiv.concat(`</select>`);
      }
      document.getElementById('ajax-content').insertAdjacentHTML('beforeend', newdiv);
    })
  </script>
  <?php
}

function list_oppgaver(){
  ?>
  <p>
    <form action="oppgave_ajax_query.php?visoppgave=true" method="post">
      <input type="radio" id="visalle" name="vis" />
      <label for="visalle">Vis alle oppgaver</label><br />
      <input type="radio" id="vismine" name="vis" />
      <label for="mine">Vis mine oppgaver</label><br />
      <input type="radio" id="faggruppe" name="vis" />
      <label for="faggruppe">Vis alle oppgaver i min faggruppe</label>
    </form>
  </p>

  <script>
    $("#visalle").change(function(){
      $.post("oppgave_ajax_query.php?visoppgave=alle", function(jsondata){
        data = JSON.parse(jsondata);
        for (const element of data) {
          console.log(element['id']);
          var line = `<p><a id="${element['id']}" class="oppgaveid">${element['emnekode']} - ${element['oppgavenr']} - ${element['tittel']}</a> Veileder: ${element['fornavn']} ${element['navn']}</p>`;
          $("#oppgaveliste").append(line);
        }
      });
    });

    $(document).on('click', '.oppgaveid', function(e) {
      var id = e.currentTarget.id;
      $('#ajax-content').load('https://org.ntnu.no/ibm/oppgaver/oppgave_ajax.php?oppgaver='.concat(id));

    });

    $("#vismine").change(function(){
      alert("Vis mine!")
    });
    $("#faggruppe").change(function(){
      alert("Vis faggruppe!")
    });
  </script>

  <?php
}

function form_ny_oppgave(){
  global $emner;
  global $brukere;
  list_emner();
  //var_dump($brukere->brukerliste);
  //var_dump($emner);
  ?>
<div class="form-group">
  <form id="nyoppgave" action="oppgave_ajax_query.php?nyoppgave=true" method="post">
    <label for="emnekode">Emnekode:</label>
    <select id="emnekode" name="emnekode" class="form-control">
      <?php
      echo "<option value=\"\"></option>";
      foreach ($emner as $row) {
        echo "<option value=\"$row[emnekode]\">$row[emnekode] $row[emnenavn]</option>";
      }
      ?>
    </select>
    <label for="veileder">Veileder:</label>
    <select id="veileder" name="veileder" class="form-control req">
      <?php
      echo "<option value=\"\"></option>";
      foreach ($brukere->brukerliste as $row) {
        echo "<option value=\"$row[id]\">$row[navn], $row[fornavn]</option>";
      }
      ?>
    </select>
    <label for="sensor1">Sensor 1 (intern):</label>
    <select id="sensor1" name="sensor1" class="form-control">
      <?php
      echo "<option value=\"\"></option>";
      foreach ($brukere->brukerliste as $row) {
        echo "<option value=\"$row[id]\">$row[navn], $row[fornavn]</option>";
      }
      ?>
    </select>
    <label for="sensor2">Sensor 2 (ekstern):</label>
    <select id="sensor2" name="sensor2" class="form-control">
      <?php
      $brukere->finn_brukere(3);
      echo "<option value=\"\"></option>";
      foreach ($brukere->brukerliste as $row) {
        echo "<option value=\"$row[id]\">$row[navn], $row[fornavn]</option>";
      }
      ?>
    </select>
    <label for="oppgavenr">Oppgavenummer:</label>
    <input type="text" class="form-control req" name="oppgavenr" id="oppgavenr" />
    <label for="tittel">Tittel:</label>
    <input type="text" class="form-control req" name="tittel" id="tittel" />
    <label for="tilknyttet_emne">Tilknyttet emne:</label>
    <input type="text" class="form-control" name="tilknyttet_emne" id="tilknyttet_emne" />
    <label for="bakgrunn">Bakgrunn:</label>
    <textarea name="bakgrunn" class="form-control"></textarea>
    <label for="beskrivelse">Beskrivelse:</label>
    <textarea name="beskrivelse" class="form-control"></textarea>
    <label for="number">Antall studenter:</label>
    <input type="number" class="form-control" name="antall_stud" id="antall_stud" />
    <label for="annen_kontakt">Annen kontakt (om forskjellig fra veileder):</label>
    <input type="text" class="form-control" name="annen_kontakt" id="annen_kontakt" />
    <label for="eksterne">Eventuell ekstern samarbeidspartner/kontakt:</label>
    <input type="text" class="form-control" name="eksterne" id="eksterne" />
    <label for="hovedprofil">Hovedprofil:</label>
    <input type="text" class="form-control" name="hovedprofil" id="hovedprofil" />
    <fieldset>
      <label>Master- og/eller prosjektoppgave?</label><br />
      <label for="master">Master</label>
      <input type="checkbox" name="master" id="master" value="1"  /><br />
      <label for="prosjekt">Prosjektoppgave</label>
      <input type="checkbox" name="prosjekt" id="prosjekt" value="1" />
    </fieldset>
    <input type="submit" value="Lagre"/> <input type="reset" />
  </form>

  <script type="text/javascript">
    var frm = $('#nyoppgave');
    var id;

    frm.submit(function (e) {

      e.preventDefault();
      var oppgavenr = $("#oppgavenr").val();
      var tittel = $("#tittel").val();
      var veileder = $("#veileder").val();

      if(oppgavenr && tittel && veileder) {

        $.ajax({
          type: frm.attr('method'),
          url: frm.attr('action'),
          data: frm.serialize(),
          success: function (data) {
            console.log('Submission was successful.');
            console.log(data);
            id = data;
            dialog(data);

          },
          error: function (data) {
            console.log('An error occurred.');
            console.log(data);
          },
        });
      }
    });

    function dialog(oppgave) {
      // alert(oppgave);
      $( "#dialog" ).dialog();
    }

  </script>
</div>
  <?php
}
?>
<html>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <style>
    .error{
      outline: 1px solid red;
    }
  </style>
  <script>
    $(document).ready(function(){
      $('.req').blur(function(){
        if(!$(this).val()){
          $(this).addClass("error");
        } else{
          $(this).removeClass("error");
        }
      });
    });
  </script>
<body>

<?php
if(isset($_GET['oppgaver']) AND $_GET['oppgaver'] == 'ny'){
  form_ny_oppgave();
}elseif(isset($_GET['oppgaver']) AND $_GET['oppgaver'] == 'alle'){
  list_oppgaver();
}elseif(isset($_GET['oppgaver'])){
  vis_oppgave($_GET['oppgaver']);
}

?>
<div id="dialog" title="Oppgaveteksten er lagret">
  <p>Ny oppgave er lagret. Vil du vise og redigere oppgave, eller vil du opprette flere oppgaver?<br />
  Bruk reset-knappen om du ikke vil gjenbruke noe av informasjonen i skjemaet.</p>
  <button id="vis">Vis oppgave</button><button id="opprett">Opprett flere oppgaver</button>
</div>
<div id="oppgaveliste"></div>
<script>
  $(document).ready(function() {
    $("#dialog").hide();
  });
  $("#opprett").click(function () {
    $( "#dialog" ).dialog( "close" );
  });
  $("#vis").click(function () {
    $('#ajax-content').load('https://org.ntnu.no/ibm/oppgaver/oppgave_ajax.php?oppgaver='.concat(id));
    $('#dialog').dialog("close");
  });
  $("#tilbake").click(function() {
    $('#ajax-content').load('https://org.ntnu.no/ibm/oppgaver/oppgave_ajax.php?oppgaver=alle');
  })
</script>
</body>
</html>
