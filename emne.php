<?php
include("config.php");
include("classes.php");
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION['access'])){
  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_level();
if($access->access_level != 1 AND $access->access_level != 2){
  header("Location: login.php");
}
if(!isset($_SESSION['vurderingsenhet'])) {
  $_SESSION['vurderingsenhet'] = new vurderingsenhet();
}
$vurderingsenhet = $_SESSION['vurderingsenhet'];
if(isset($_GET['id'])){
  $vurderingsenhet->emnekode = $_GET['id'];
}
if(isset($_GET['terminid'])) {
  $vurderingsenhet->termin = $_GET['terminid'];
}
if(isset($_GET['vurdid'])){
  $vurderingsenhet->id = $_GET['vurdid'];
}

$vurderingsenhet->list_meldinger();


if(isset($_GET['submitform'])){
  $vurderingsenhet->lagre_endringer();
  //var_dump($_POST);
  if(!isset($_GET['ajax'])) {
    header("Location: emne.php");
  }
  //$keys = array_keys($_POST);
  //var_dump($keys);
}
?>
<script src="js/sorttable.js"></script>
<div>
  <p>
  <h1><?php echo $vurderingsenhet->emnekode ?></h1>
  <p>Ulagrede endringer markeres i rødt. Lagre-knapp nederst</p>
  <p>Grønne linjer indikerer at kommisjonen er opprettet i Inspera og sensorene skal ha tilgang, disse kan ikke redigeres</p>
  <p>
  <?php
  echo($vurderingsenhet->emnetekst);
  ?>
  </p>
  <table class="table table-hover my-info sortable">
    <tr>
      <th>Studnr.</th>
      <th>Navn</th>
      <th>Epost</th>
      <th>Alle veiledere</th>
      <th>Veileder</th>
      <th>Ekstern sensor1</th>
      <th>Sensor1 (intern)</th>
      <th>sensor2 (ekstern)</th>
      <th>Gruppe</th>
      <?php
      if ($access->access_level == 1){
        echo "<th>låst</th>";
      }else{
        echo "<th style='display: none;'>låst</th>";
      }
      ?>
    </tr>
  <?php
  foreach ($vurderingsenhet->studentliste as $student){
    if($student['locked']){
      echo "<tr id=\"{$student['studentid']}-row\" class=\"locked\">\r\n";
    }else{
      echo "<tr id=\"{$student['studentid']}-row\">\r\n";
    }
    echo "\t<th scope=\"row\">" . $student['studentid'] . "</th>\r\n";
    echo "\t<td><a href='#' onclick=\"$('#ajax-content').load('student.php?studentid=" . $student['studentid'] . "')\">" . $student['navn'] . "</a></td>\r\n";
    echo "\t<td>" . $student['epost'] . "</td>\r\n";
    echo "\t<td>\r\n\t\t<select form=\"lagre\" name=\"". $student['studentid'] ."-veileder_alle\" onchange=\"veilederExInt(this.value, document.getElementById('". $student['studentid'] ."-veileder'), '". $vurderingsenhet->id ."') \">\r\n";
    if($student['veileder_alle']) {
      echo "\t\t\t<option name=\"veileder_alle\" form=\"lagre\" value=\"1\">Vis alle</option>\r\n";
      echo "\t\t\t<option name=\"veileder_alle\" form=\"lagre\" value=\"0\">Vis egne</option>\r\n";
    } else {
      echo "\t\t\t<option name=\"veileder_alle\" form=\"lagre\" value=\"0\">Vis egne</option>\r\n";
      echo "\t\t\t<option name=\"veileder_alle\" form=\"lagre\" value=\"1\">Vis alle</option>\r\n";
    }
    echo "\t\t</select>\r\n\t</td>";

    echo '<td sorttable_customkey="'. $vurderingsenhet->idToName($student['veileder'], "veileder").'"><select form="lagre" name="' . $student['studentid'] .'-veileder" id="' . $student['studentid'] .'-veileder">';
    echo "<option name=\"veileder\" form=\"lagre\" value=\"\"></option>\r\n";
    foreach($vurderingsenhet->brukerliste as $bruker){
      if($bruker['id'] == $student['veileder']){
        echo "<option form=\"lagre\" value=\"". $bruker['id'] ."\" selected>$bruker[navn], $bruker[fornavn]</option>\r\n";
      }else{
        if($student['veileder_alle'] OR $bruker['faggruppe'] == $vurderingsenhet->faggruppe) {
          echo "<option form=\"lagre\" value=\"$bruker[id]\">$bruker[navn], $bruker[fornavn]</option>\r\n";
        }
      }
    }
    echo "</select>\r\n</td>\r\n \r\n";

    echo "<td>\r\n";

    echo "\t<select form=\"lagre\" name=\"". $student['studentid'] ."-toeksterne\" onchange=\"replaceSelector(this.value, document.getElementById('". $student['studentid'] ."-sensor1')) \">\r\n";
    if($student['toeksterne'] == 0) {
      echo '<option name="toeksterne" form="lagre" value="0">Intern</option>';
      echo '<option name="toeksterne" form="lagre" value="1">Ekstern</option>';
    } else {
      echo '<option name="toeksterne" form="lagre" value="1">Ekstern</option>';
      echo '<option name="toeksterne" form="lagre" value="0">Intern</option>';
    }
    echo "\t</select>\r\n";
    echo "</td>\r\n";

    if($student['toeksterne'] == 0) {
      echo '<td sorttable_customkey="'. $vurderingsenhet->idToName($student['sensor1'], "veileder").'"><select form="lagre" name="' . $student['studentid'] . '-sensor1" id="' . $student['studentid'] . '-sensor1">';
      echo "<option form=\"lagre\" value=\"\"></option>\r\n";
      foreach ($vurderingsenhet->brukerliste as $bruker) {
        if ($bruker['id'] == $student['sensor1']) {
          echo "<option form=\"lagre\" value=\"" . $bruker['id'] . "\" selected>$bruker[navn], $bruker[fornavn]</option>\r\n";
        } else {
          echo "<option form=\"lagre\" value=\"$bruker[id]\">$bruker[navn], $bruker[fornavn]</option>\r\n";
        }
      }
      echo "</select>\r\n</td>";

    } else{
      echo '<td sorttable_customkey="'. $vurderingsenhet->idToName($student['sensor1'], "sensor").'"><select form="lagre" name="' . $student['studentid'] .'-sensor1">';
      echo "<option form=\"lagre\" value=\"\"></option>\r\n";
      foreach($vurderingsenhet->ekstern_sensor as $bruker){
        if($bruker['id'] == $student['sensor1']){
          echo "<option form=\"lagre\" value=\"". $bruker['id'] ."\" selected>$bruker[navn], $bruker[fornavn]</option>\r\n";
        } else{
          echo "<option form=\"lagre\" value=\"$bruker[id]\">$bruker[navn], $bruker[fornavn]</option>\r\n";
        }
      }
      echo "</select>\r\n</td>\r\n\r\n";
    }

    echo "<td sorttable_customkey=\"". $vurderingsenhet->idToName($student['sensor2'], 'sensor')."\"><select form=\"lagre\" name=\"{$student['studentid']}-sensor2\" >\r\n";
    echo "<option form=\"lagre\" value=\"\"></option>\r\n";
    foreach($vurderingsenhet->ekstern_sensor as $bruker){
      if($bruker['id'] == $student['sensor2']){
        echo "\t\t<option form=\"lagre\" value=\"". $bruker['id'] ."\" selected>$bruker[navn], $bruker[fornavn]</option>\r\n";
      } else{
        echo "\t\t<option form=\"lagre\" value=\"$bruker[id]\">$bruker[navn], $bruker[fornavn]</option>\r\n";
      }
    }
    echo "\r\n\t</select></td>\r\n";
    echo "<td><input form='lagre' type='number' name='$student[studentid]-gruppe' value=$student[gruppe]></td>\r\n";
    if ($access->access_level == 1){
      echo "<td>";
      if($student['locked']) {
        echo "<select form='lagre' name='{$student['studentid']}-locked' onchange='locked({$student['studentid']})'>\r\n";
        echo "\t<option value='0'>ulåst</option>\r\n";
        echo "\t<option value='1' selected='selected'>låst</option>\r\n";
        echo "</select>\r\n";
      }else{
        echo "<select form='lagre' name='{$student['studentid']}-locked' onchange='locked({$student['studentid']})'>\r\n";
        echo "\t<option value='0' selected='selected'>ulåst</option>\r\n";
        echo "\t<option value='1'>låst</option>\r\n";
        echo "</select>\r\n";
      }
      echo "</td>";
    }else{
      echo "<td style='display: none'>";
      if($student['locked']) {
        echo "<select form='lagre' name='{$student['studentid']}-locked' onchange='locked({$student['studentid']})'>\r\n";
        echo "\t<option value='0'>ulåst</option>\r\n";
        echo "\t<option value='1' selected='selected'>låst</option>\r\n";
        echo "</select>\r\n";
      }else{
        echo "<select form='lagre' name='{$student['studentid']}-locked' onchange='locked({$student['studentid']})'>\r\n";
        echo "\t<option value='0' selected='selected'>ulåst</option>\r\n";
        echo "\t<option value='1'>låst</option>\r\n";
        echo "</select>\r\n";
      }
    }
    echo "</tr>\r\n";
    echo "<script>locked({$student['studentid']});</script>\r\n";
  }
  ?>
  </table>
  <form action="emne.php?submitform=true" id="lagre" method="post" data-autosubmit>
    <input type="submit" value="Lagre endringer" />
    <input type="reset" value="Angre endringer"/>
  </form>

  </p>
  <p><a href="https://hjelp.ntnu.no/tas/public/ssp/content/serviceflow?unid=bdfe20c8fa764d6099cd889e893daa78">Legg til ny ekstern sensor</a></p>
  <p><a href="index.php">Tilbake</a></p>
</div>
<script>
  $(document).ready(function() {
    $('input, select, textarea').on('change', function() {
      $(this).addClass('changed');
    });

    $('form').on('submit', function() {
      $('select:not(.changed)').prop('disabled', true);
    });
  });
  $(function() {
    $('form[data-autosubmit]').autosubmit();
  });

</script>
  <!-- <p><a href="student.php?nystudent=true">Legg til student</a></p> -->

