<?php
include('config.php');
include('classes.php');
$sql = new sql();
$qry = "SELECT emner.emnekode, emner.emnenavn, faggrupper.forkortelse, brukere.* FROM emner ";
$qry .= "INNER JOIN faggrupper ON emner.faggruppe=faggrupper.id ";
$qry .= "INNER JOIN brukere ON emner.emneansvarlig=brukere.id";
$result = $sql->selectQuery($qry);
?>
<table border="1">
  <thead>
    <tr>
      <th>Emnekode</th>
      <th>Emnenavn</th>
      <th>Etternavn</th>
      <th>Fornavn</th>
      <th>Epost</th>
      <th>Faggruppe</th>
    </tr>
  </thead>
  <tbody>
  <?php
  foreach ($result as $row){
  ?><tr>
      <td><?= $row['emnekode'] ?></td>
      <td><?= $row['emnenavn'] ?></td>
      <td><?= $row['navn'] ?></td>
      <td><?= $row['fornavn'] ?></td>
      <td><?= $row['email'] ?></td>
      <td><?= $row['forkortelse'] ?></td>
    </tr>
    <?php
    }
  ?>
  </tbody>
</table>
