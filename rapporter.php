<?php
include("config.php");
include("classes.php");
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION['access'])){
  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_level();
if($access->access_level != 1 AND $access->access_level != 2){
  header("Location: index.php");
}

?>
<script src="js/rapporter.js"></script>
<script src="js/sorttable.js"></script>
<h1>Oversikt over alle dine studentoppgaver</h1>
<p>
  <label for="filterFagperson">Vis oppgaver hvor jeg er: <select id="filterFagperson">
      <option value="veileder" selected>Veileder</option>
      <option value="sensor">Sensor</option>
    </select>
  </label>
  <label for="ddlTermin">Termin: <select id="ddlTermin">
      <option value="alle" selected>Alle</option>
    </select></label>
  <button id="btnHent" onclick="hentRapport(<?php echo $access->current_user; ?>)">Hent</button>
</p>

<div id="studentliste">
  <table class="table table-hover my-info sortable" id="studentTabell">
    <thead>
    <tr id="hRow">
      <th id="hEmnekode">Emnekode</th>
      <th id="hTermin">Termin</th>
      <th id="hNavn">Navn</th>
      <th id="hEpost">Epost</th>
      <th id="hVeileder">Veileder</th>
      <th id="hSensor1">Sensor1 (intern)</th>
      <th id="hSensor2">sensor2 (ekstern)</th>
    </tr>
    </thead>
    <tbody id="studentListeTbody"></tbody>
  </table>
</div>

<script>
  function keepAlive() {
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', "emner_ajax.php?keepalive=true", true);
    httpRequest.send(null);
  }

  setInterval(keepAlive, 840000);
</script>
