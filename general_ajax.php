<?php
require_once ('classes.php');
require_once ('config.php');
if(isset($_GET['getURL'])){
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $_GET['getURL']);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $webPage = curl_exec($ch);
  curl_close($ch);
  echo $webPage;
}
if(isset($_GET['setEmneansvarlig'])){
//  var_dump($_GET);
//  var_dump($_GET['emnekode']);
//  var_dump($_GET['emneansvarlig']);
  $sensor = new klagesensur;
  $result = $sensor->setEmneansvarlig($_GET['emnekode'], $_GET['emneansvarlig']);
  var_dump($result);
}
if(isset($_GET['getEmnelisteAlle'])){
  $sensor = new klagesensur();
  if($_GET['getEmnelisteAlle'] == 'all'){
    echo json_encode($sensor->getAllCourses(1));
  }else{
    echo json_encode($sensor->getAllCourses());
  }
}
if(isset($_GET['getFaggruppeAktive'])){
  $sensor = new klagesensur();
  echo json_encode($sensor->getAllFaggruppe());
}
if(isset($_GET['getBrukereAktive'])){
  $sensor = new klagesensur();
  $result = json_encode($sensor->getAllEmneansvarlig());
  if(empty($result)){
    echo "{}";
  }else{
    echo $result;
  }
}
if(isset($_GET['getSensorEkstern'])){
  $sensor = new klagesensur();
  $result = json_encode($sensor->getEkstern($_GET['getSensorEkstern']));
  echo $result;
}

if(isset($_GET['getSensorIntern'])){
  $sensor = new klagesensur();
  $result = $sensor->getIntern($_GET['getSensorIntern']);
  echo json_encode($result);
  //var_dump($result);

}
if(isset($_GET['setSensorEkstern'])){
  $sensor = new klagesensur();
  $result = json_encode($sensor->settEksternSensor($_GET['setSensorEkstern'], $_GET['emnekode']));
  echo $result;
}

if(isset($_GET['setSensorIntern'])){
  $sensor = new klagesensur();
  $result = json_encode($sensor->settInternSensor($_GET['setSensorIntern'], $_GET['emnekode']));
  echo $result;

}

if(isset($_GET['getAlleEksternSensor'])){
  $sensor = new klagesensur();
  echo json_encode($sensor->getAlleEksternSensor());
}
if(isset($_GET['session'])){
  var_dump($_SESSION);
  echo $_SESSION['access']->current_user;
}
if(isset($_GET['deleteSensor'])){
  $sensor = new klagesensur();
  echo json_encode($sensor->slettSensor($_GET['deleteSensor'], $_SESSION['access']->current_user, $_GET['emnekode'], $_GET['intext']));
}
if(isset($_GET['emnetyper'])){
  $sensor = new klagesensur();
  echo json_encode($sensor->getEmnetyper());
}
if(isset($_GET['nyttEmne'])){
  $admin = new emneadministrasjon();
  echo json_encode($admin->nyttEmne($_POST));
}
if(isset($_GET['slettEmne'])){
  $admin = new emneadministrasjon();
  echo json_encode($admin->slettEmne($_GET['slettEmne']));
}
if(isset($_GET['updateCourse'])){
  $admin = new emneadministrasjon();
  echo json_encode($admin->lagreEmne($_POST));
}
if(isset($_GET['newFagperson'])){
  $admin = new fagpersonadmin();
  echo json_encode($admin->newFagperson($_POST));
}
if(isset($_GET['saveFagperson'])){
  $admin = new fagpersonadmin();
  echo json_encode($admin->saveFagperson($_POST));
}
if(isset($_GET['deleteFagperson'])){
  $admin = new fagpersonadmin();
  echo json_encode($admin->deleteFagperson($_GET['deleteFagperson']));
}
if(isset($_GET['getBrukereAlle'])){
  $sensor = new fagpersonadmin();
  $result = json_encode($sensor->getAllEmneansvarlig());
  if(empty($result)){
    echo "{}";
  }else{
    echo $result;
  }
}
if(isset($_GET['getFaggruppeAlle'])){
  $sensor = new emneadministrasjon();
  echo json_encode($sensor->getAllFaggruppe());
}
