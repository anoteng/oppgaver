const fagperson = {
  fagpersoner: {},
  faggrupper:{},
  nodelist: {},
  hidden: [],

  initialize: function (){
    this.hentFagpersoner()
      .then(()=>{
        this.hentFaggrupper()
          .catch(error => alert(error))
      })
      .catch(error => alert(error))
  },
  setEventListeners: function(){
    const that = this;
    const filterFaggruppe = document.querySelector('#filterFaggruppe');
    console.log(filterFaggruppe);
    filterFaggruppe.addEventListener('change', e => {
      this.filternodes(e.currentTarget);
    })
    $('#filterEmneansvarlig').on('select2:select', e => {
      that.filternodes(e.currentTarget);
    });

    const buttons = document.querySelectorAll('.btn');
    buttons.forEach(function (e){
      let btnType = e.id.split("-")[0];
      let emnekode = e.id.split("-")[1];
      if(btnType == 'nyBtnSave'){
        e.addEventListener('click', (e)=>{
          this.btnSaveNew(e);
        })
      }else{
        e.addEventListener('click', (e)=>{
          this.btnClicker(e);
        })
      }
    }, this)
  },

  btnSaveNew: function (e){
    const etternavn = document.querySelector('#nyEtternavn');
    const fornavn = document.querySelector('#nyFornavn');
    const epost = document.querySelector('#nyEpost');
    const faggruppe = document.querySelector('#nyFaggruppe');
    const tilgang = document.querySelector('#nyTilgang');
    const aktiv = document.querySelector('#nyAktiv');
    const values = {
      etternavn: etternavn.value,
      fornavn: fornavn.value,
      epost: epost.value,
      faggruppe: faggruppe.value,
      tilgang: tilgang.value,
      aktiv: aktiv.checked
    }
    const formData = new FormData;
    formData.append('etternavn', etternavn.value)
    formData.append('fornavn', fornavn.value)
    formData.append('epost', epost.value)
    formData.append('faggruppe', faggruppe.value)
    formData.append('tilgang', tilgang.value)
    formData.append('aktiv', aktiv.checked)
    if(etternavn && fornavn && epost && faggruppe && tilgang){
      fetch('general_ajax.php?newFagperson', {
        method: 'post',
        body: formData
      })
        .then(result => result.json())
        .then(data => this.newNode(data, values))
        .then(()=> {
          etternavn.value = '';
          fornavn.value = '';
          epost.value = '';
          faggruppe.value = '';
          tilgang.value = '';
          aktiv.value = '';

        })
        .catch(error => alert(error))

    }else{
      alert('Alle feltene må fylles ut');
    }

  },

  btnClicker: function (e){
    const type = e.currentTarget.id.split('-')[0];
    const id = e.currentTarget.id.split('-')[1];
    switch (type) {
      case 'btnSave':
        this.btnSaveChanged(id);
        break;

      case 'btnEdit':
        this.btnEdit(id);
        break;

      case 'btnTransfer':
        this.btnTransfer();
        break;

      case 'btnDelete':
        this.btnDelete(id);
    }
  },

  btnSaveChanged: function(id){
    const btnEdit = document.querySelector(`#btnEdit-${id}`);
    const btnLagre = document.querySelector(`#btnSave-${id}`);
    const formData = new FormData;
    const nodes = this.getChildNodes(id);
    formData.append('id', id)
    formData.append('fornavn', nodes.fornavn.value)
    formData.append('etternavn', nodes.etternavn.value)
    formData.append('epost', nodes.epost.value)
    formData.append('faggruppe', nodes.faggruppe.value)
    formData.append('tilgang', nodes.tilgang.value)
    formData.append('aktiv', nodes.aktiv.checked)

    fetch('general_ajax.php?saveFagperson', {
      method: 'post',
      body: formData
    })
      .then ((response)=> response.json())
      .then(()=>{
        btnEdit.classList.remove('hidden');
        btnLagre.classList.add('hidden')
        const row = document.querySelector(`#row-${id}`)

        row.classList.add('lagret');
        setTimeout(function(){
          row.classList.remove('lagret');
        }, 4000)
        nodes.fornavn.setAttribute('readonly', true);
        nodes.etternavn.setAttribute('readonly', true);
        nodes.faggruppe.setAttribute('disabled', true);
        nodes.epost.setAttribute('readonly', true);
        nodes.aktiv.setAttribute('disabled', true);
        nodes.tilgang.setAttribute('disabled', true);
      })
      .catch(error => console.log(error))

  },
  btnEdit: function(id){
    const nodes = this.getChildNodes(id);
    //console.log(nodes);
    nodes.fornavn.removeAttribute('readonly');
    nodes.etternavn.removeAttribute('readonly');
    nodes.faggruppe.removeAttribute('disabled');
    nodes.epost.removeAttribute('readonly');
    nodes.aktiv.removeAttribute('disabled');
    nodes.tilgang.removeAttribute('disabled');
    const btnEdit = document.querySelector(`#btnEdit-${id}`);
    const btnLagre = document.querySelector(`#btnSave-${id}`);
    btnEdit.classList.add('hidden');
    btnLagre.classList.remove('hidden')
  },
  btnTransfer: function(id){
    alert('Denne funksjonen er ikke implementert ennå, det er bare å glede seg til den kommer:-D')
  },
  btnDelete: function(id){
    if(confirm('ADVARSEL! Du bør kun slette fagpersoner som er lagt inn ved en feil, fagpersoner som har sluttet skal deaktiveres, ikke slettes.')){
      if(confirm('Er du HELT SIKKER på at du vil slette denne fagpersonen?')){
        fetch('general_ajax.php?deleteFagperson=' + id)
          .then(result => result.json())
          .then(result => this.removeNode(result))
          .catch(error => alert(error))
      }
    }
  },

  removeNode: function(id){
    const node = document.querySelector('#row-' + id)
    const parent = node.parentElement;
    // console.log(parent)
    parent.removeChild(node);
    // delete this.nodelist[id]
  },

  newNode: function(id, values){
    const template = document.querySelector('#fagpersonTemplate').cloneNode(true);
    template.id = `${id}-row`;
    template.classList.remove('hidden');
    template.classList.add('lagret');
    const fldEtternavn = template.querySelector('#templateEtternavn');
    const fldFornavn = template.querySelector('#templateFornavn');
    const fldEpost = template.querySelector('#templateEpost');
    const fldFaggruppe = template.querySelector('#templateFaggruppe');
    const fldTilgang = template.querySelector('#templateTilgang');
    const fldAktiv = template.querySelector('#templateAktiv');
    fldEtternavn.value = values.etternavn;
    fldFornavn.value = values.fornavn;
    fldEpost.value = values.epost;
    fldFaggruppe.value = values.faggruppe;
    fldTilgang.value = values.tilgang;
    fldEtternavn.id = `etternavn-${id}`;
    fldFornavn.id = `fornavn-${id}`
    fldEpost.id = `epost-${id}`
    fldFaggruppe.id = `faggruppe-${id}`
    fldTilgang.id = `tilgang-${id}`
    fldAktiv.id = `aktiv-${id}`
    if(values.aktiv){
      fldAktiv.setAttribute('checked', values.aktiv);
    }else{
      fldAktiv.removeAttribute('checked');
    }


    const btnLagre = template.querySelector('#btnSave-template');
    const btnEdit = template.querySelector('#btnEdit-template');
    const btnDelete = template.querySelector('#btnDelete-template');
    const btnTransfer = template.querySelector('#btnTransfer-template');
    const buttons = [btnLagre, btnEdit, btnDelete, btnTransfer];
    buttons.forEach(function(element){
      const type = element.id.split('-')[0];
      element.id = `${type}-${id}`;
      element.addEventListener('click', function(e){
        this.btnClicker(e);
      })
    }, this)

    const fagpersonliste = document.querySelector('#fagpersonliste')
    fagpersonliste.insertAdjacentElement('afterbegin', template)
    setTimeout(function () {
      template.classList.remove('lagret');
    }, 4000);

  },

  filternodes: function(by){
    //console.log(by.value);
    for(let item of Object.keys(this.nodelist)){
      console.log(item)
      if(by.id == 'filterEmneansvarlig'){
        if(this.fagpersoner[item].id == by.value.toString()){
          this.nodelist[item].classList.remove('hidden');
          // console.dir(this.nodelist[item])
        }else if(by.value.toString() == 'null'){
          this.nodelist[item].classList.remove('hidden');
        }else{
          // console.dir(item, this.nodelist[item])
          this.nodelist[item].classList.add('hidden');
        }
      }else{
        if(this.fagpersoner[item].faggruppe == by.value.toString()){
          this.nodelist[item].classList.remove('hidden');
        }else if(by.value.toString() == 'null'){
          this.nodelist[item].classList.remove('hidden');
        }else{
          this.nodelist[item].classList.add('hidden');
        }
      }
    }
  },

  hentFagpersoner: function(){
    promise = new Promise((resolve, reject) => {
      fetch('general_ajax.php?getBrukereAlle')
        .then(response => response.json())
        .then(data =>{
          for(let fagperson of data){
            this.fagpersoner[fagperson.id] = fagperson;
          }
        })
        .then(resolve('Success!'))
        .catch(error => reject(error))
    })
    return promise;
  },

  hentFaggrupper: function(){
    promise = new Promise((resolve, reject) => {
      fetch('general_ajax.php?getFaggruppeAktive')
        .then(response => response.json())
        .then(data =>{
          for(let faggruppe of data){
            this.faggruppeliste[faggruppe.id] = faggruppe;
          }
        })
        .then(resolve('Success!'))
        .catch(error => reject(error))
    })
    return promise;
  },

  getNodes: function() {
    let fagpersonliste = document.querySelector('#fagpersonliste');
    let nodes = fagpersonliste.querySelectorAll('div.row.rowhover');

    nodes.forEach(function (c) {
      let id = c.id;
      let fagpersonId = id.split('-')[1];
      this.nodelist[fagpersonId] = c;
    }, this)
  },

  getChildNodes: function(id){
    let etternavn = document.querySelector(`#etternavn-${id}`);
    let navn = document.querySelector(`#fornavn-${id}`);
    let epost = document.querySelector(`#epost-${id}`);
    let faggruppe = document.querySelector(`#faggruppe-${id}`);
    let tilgang = document.querySelector(`#tilgang-${id}`);
    let aktiv = document.querySelector(`#aktiv-${id}`);
    let childnodes = {
      etternavn: etternavn,
      fornavn: navn,
      epost: epost,
      faggruppe: faggruppe,
      tilgang: tilgang,
      aktiv: aktiv
    };
    return childnodes;
  },
}
