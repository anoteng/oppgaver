function fillStudentTable(id){
  let xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      let response = JSON.parse(this.responseText);
      studTblHeader(response);

    }
  };
  xmlhttp.abort();
  xmlhttp.open("GET", `student_ajax.php?studentinfo=${id}`, true);
  xmlhttp.send();

}
function studTblHeader(response){

  let tblStudNavn = document.getElementById("studNavn");
  let tblStudEpost = document.getElementById("studEpost");
  let ddlVurdMeldinger = document.getElementById("ddlVurdmelding");
  tblStudNavn.innerHTML = response[0]['navn'];
  tblStudEpost.innerHTML = response[0]['epost'];
  let i;
  let option;
  let value;
  let text;
  for(i = 0; i<response.length; i++){
    option = document.createElement("option");
    value = response[i]['vurderingsenhet'];
    text = `${response[i]['emne']}-${response[i]['year']}-${response[i]['month']}`;
    option.value = value;
    option.text = text;
    ddlVurdMeldinger.options.add(option);
  }
}

function addRow(frmElement, col1, col2){
  let row = document.createElement("tr");
  let cell1 = row.insertCell(0);
  let cell2 = row.insertCell(1);
  cell1.innerHTML = col1;
  cell2.innerHTML = col2;
  frmElement.appendChild(cell1);
  frmElement.appendChild(cell2)
}

function fillStudTblBody(response){
  let tblBody = document.getElementById("tblBodyStudentinfo");
  addRow(tblBody, 'Veileder', response[0]['veileder']);
}

function studTblBody(vurdid, studid){
  let xmlhttp = new XMLHttpRequest();
  let response;

  xmlhttp.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      response = JSON.parse(this.responseText);
      fillStudTblBody(response);
    }
  };
  xmlhttp.abort();
  xmlhttp.open("GET", `student_ajax.php?studentinfo=${studid}&vurdmelding=${vurdid}`, true);
  xmlhttp.send();
}
