const ajaxContent = document.querySelector('#ajax-content');
const spinner = document.querySelector('#spinner');
(function($) {
  $.fn.autosubmit = function() {
    this.submit(function(event) {
      event.preventDefault();
      let form = $(this);
      $.ajax({
        type: form.attr('method'),
        url: form.attr('action') + 'ajax=true',
        data: form.serialize()
      }).done(function(data) {
        $('.changed').addClass('lagret');
        $('select:not(.changed)').prop('disabled', false);
        $('select.locked').prop('disabled', true);
        $('.changed').removeClass('changed');
      }).fail(function(data) {
        alert(data);
      });
    });
    return this;
  };
})(jQuery)


function nySensor(tbl){
  let xmlhttp = new XMLHttpRequest();
  let navn = document.getElementById("ny-navn").value;
  let email = document.getElementById("ny-email").value;
  let fornavn = document.getElementById("ny-fornavn").value;

  xmlhttp.onreadystatechange = function() {

    if (this.readyState === 4 && this.status === 200) {
      let response = JSON.parse(this.responseText);
      // console.log(response)

      let i;
      for(i=0; i < response.length; i++){
        let c = document.createElement("tr");
        let d = document.createElement("td");
        let e = document.createElement("input");
        e.type = "text";

        e.id = response[i].id;
        e.name = response[i].id;
        e.value = response[i].id;
        e.classList.add("lagret");
        d.appendChild(e);
        c.appendChild(d);

        d = document.createElement("td");
        e = document.createElement("input");
        e.type = "text";
        e.name = response[i].id + '-email';
        e.id = response[i].id + '-email';
        e.value = response[i].email;
        e.classList.add("lagret");
        d.appendChild(e);
        c.appendChild(d);

        d = document.createElement("td");
        e = document.createElement("input");
        e.type = "text";
        e.name = response[i].id + '-navn';
        e.id = response[i].id + '-navn';
        e.value = response[i].navn;
        e.classList.add("lagret");
        d.appendChild(e);
        c.appendChild(d);

        d = document.createElement("td");
        e = document.createElement("input");
        e.type = "text";
        e.name = response[i].id + '-fornavn';
        e.id = response[i].id + '-fornavn';
        e.value = response[i].fornavn;
        e.classList.add("lagret");
        d.appendChild(e);
        c.appendChild(d);
        tbody = tbl.getElementsByTagName("tbody")[0];
        tbody.insertBefore(c, tbody.childNodes[4]);
        document.getElementById("ny-email").value = "";
        document.getElementById("ny-navn").value = "";
        document.getElementById("ny-fornavn").value = "";

      }
    }
  };
  xmlhttp.abort();
  xmlhttp.open("GET", "ny_sensor_ajax.php?nysensor=true&epost=" + email + "&navn=" + navn + "&fornavn=" + fornavn, true);
  xmlhttp.send();
}
function veilederExInt(exint, frmElement, vurdID){
  let xmlhttp = new XMLHttpRequest();
  let c;
  xmlhttp.onreadystatechange = function() {

    if (this.readyState == 4 && this.status == 200) {
      response = JSON.parse(this.responseText);
      while (frmElement.options.length > 0){
        frmElement.options.remove(0);
      }
      let c = document.createElement("option");

      c.value = "";
      c.text = "";
      frmElement.options.add(c);

      let i;
      for(i=0; i < response.length; i++){
        let c = document.createElement("option");
        c.value = response[i].id;
        c.text = response[i].navn + ", " + response[i].fornavn;
        frmElement.options.add(c, i);
      }
      c = document.createElement("option");

      c.value = "";
      c.text = "";
      frmElement.options.add(c);
    }
  };

  if (exint === 0){

    xmlhttp.abort();
    xmlhttp.open("GET", `emner_ajax.php?hentveileder=intern&vurderingsenhet=${vurdID}`, true);
    xmlhttp.send();

  }else{

    xmlhttp.abort();
    xmlhttp.open("GET", "emner_ajax.php?hentveileder=ekstern", true);
    xmlhttp.send();

  }
}
function replaceSelector(exint, frmElement){
  let xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange = function() {

    if (this.readyState === 4 && this.status === 200) {
      response = JSON.parse(this.responseText);
      while (frmElement.options.length > 0){
        frmElement.options.remove(0);
      }

      let i;
      for(i=0; i < response.length; i++){
        let f = document.createElement("option");
        f.value = response[i].id;
        f.text = response[i].navn + ", " + response[i].fornavn;
        frmElement.options.add(f, i);
      }
      let c = document.createElement("option");

      c.value = "";
      c.text = "";
      c.selected = true;
      frmElement.options.add(c, 0);
    }
  };

  if (exint === 0){

    xmlhttp.abort();
    xmlhttp.open("GET", "emner_ajax.php?hentsensor=intern&vurderingsenhet=<?php echo $vurderingsenhet->id ?>", true);
    xmlhttp.send();

  }else{

    xmlhttp.abort();
    xmlhttp.open("GET", "emner_ajax.php?hentsensor=ekstern", true);
    xmlhttp.send();

  }
}

function locked(id) {
  console.log("Funksjon locked med id " + id);
  lock = document.getElementsByName(id + '-locked')[0];
  veileder = document.getElementsByName(id + '-veileder')[0];
  sensor1 = document.getElementsByName(id + '-sensor1')[0];
  sensor2 = document.getElementsByName(id + '-sensor2')[0];
  veileder_alle = document.getElementsByName(id + '-veileder_alle')[0];
  toeksterne = document.getElementsByName(id + '-toeksterne')[0];
  lockStatus = lock.value;
  row = document.getElementById(id + '-row');

  if (lockStatus == 1){
    veileder.disabled = true;
    veileder.classList.add("locked");
    sensor1.disabled = true;
    sensor1.classList.add("locked");
    sensor2.disabled = true;
    sensor2.classList.add("locked");
    veileder_alle.disabled = true;
    veileder_alle.classList.add("locked");
    toeksterne.disabled = true;
    toeksterne.classList.add("locked");
    row.classList.add("locked");
  } else {
    veileder.disabled = false;
    veileder.classList.remove("locked");
    sensor1.disabled = false;
    sensor1.classList.remove("locked");
    sensor2.disabled = false;
    sensor2.classList.remove("locked");
    veileder_alle.disabled = false;
    veileder_alle.classList.remove("locked");
    toeksterne.disabled = false;
    toeksterne.classList.remove("locked");
    row.classList.remove("locked");
  }
}
const klagesensur = () =>{
  fetch('klagesensur.php')
    .then(result => result.text())
    .then(body => {
      ajaxContent.innerHTML = body;
      sensorliste.initialize();
    })

}
const loadEmneadministrasjon = () =>{
  fetch('emneadministrasjon.php')
    .then(result => result.text())
    .then(body => {
      ajaxContent.innerHTML = body;
    })
    .then(()=>{
      $(document).ready(function() {
        $('.js-example-basic-single').select2();
      });
    })
    .finally(() =>
      $(document).ready(function() {
        emneadministrasjon.initialize();
      }));

}
const fagpersonadmin = () =>{
  fetch('fagpersonadmin.php')
    .then(result => result.text())
    .then(body => {
      ajaxContent.innerHTML = body;
    })
    .then(()=>{
      $(document).ready(function() {
        $('.js-example-basic-single').select2();
        fagperson.setEventListeners();
        fagperson.getNodes();
      })
    })
  fagperson.initialize();
}
const clickedLink = (e) => {
  if (e.currentTarget.id === 'klagesensurLink'){
    //load module klagesensur
    console.log('Loading module Klagesensur');
    klagesensur();
    document.querySelector('#semestervelgerDiv').classList.add('hidden');
  }else if(e.currentTarget.id === 'emneadministrasjon') {
    console.log('Loading module emneadministrasjon');
    loadEmneadministrasjon();
    document.querySelector('#semestervelgerDiv').classList.add('hidden');
  }else if(e.currentTarget.id === 'fagpersonadministrasjon') {
    console.log('Loading module fagpersonadministrasjon');
    fagpersonadmin();
    document.querySelector('#semestervelgerDiv').classList.add('hidden');
  }else{
    document.querySelector('#semestervelgerDiv').classList.remove('hidden');
  }
}

const allNavLinks = document.querySelectorAll('.nav-link');
for (let link of allNavLinks){
  link.addEventListener('click', clickedLink);
}
function sortHTML(parent, configObject) {

  var i = 0,
    tnode = null,
    items = [],
    item = null,
    config = configObject || {},
    children = parent.children,
    children_length = children.length,
    valuetype = (typeof config.valuetype !== "undefined") ? config.valuetype : "string",
    ascending = (typeof config.asscending !== "undefined") ? config.ascending : true,
    getValue = config.getValue || function (elem) {
      return elem.innerHTML;
    },
    comparator = config.comparator || getComparator(valuetype, ascending),
    documentFragment = null;
  for (i = 0; i < children_length; i++) {
    tnode = children[i];
    item = {
      node: tnode,
      value: getValue(tnode)
    };
    items.push(item);
  }
  items.sort(comparator);
  documentFragment = document.createDocumentFragment()
  for (var i = 0; i < items.length; i++) {
    documentFragment.appendChild(items[i].node);
  }
  parent.appendChild(documentFragment);
}

function getComparator(vtype, ascending) {
  if (vtype === "number" && ascending) {
    return function (a, b) {
      return a.value - b.value;
    }
  } else if (vtype === "number" && !ascending) {
    //parameter switch so code can be identical
    return function (b, a) {
      return a.value - b.value;
    }
  } else if (vtype === "string" && ascending) {
    return function (a, b) {
      if (a.value < b.value) {
        return -1;
      } else if (a.value === b.value) {
        return 0;
      } else {
        return 1;
      }
    }
  } else if (vtype === "string" && !ascending) {
    return function (a, b) {
      if (a.value < b.value) {
        return -1;
      } else if (a.value === b.value) {
        return 0;
      } else {
        return 1;
      }
    }
  } else {
    return null;
  }
}

let termselector = document.getElementById('termselector');

termselector.addEventListener('change', function(){
  let ajaxContent = document.getElementById('ajax-content');
  fetch('veileder.php')
    .then(function(response){
      return response.text();
    })
    .then(function(body){
      ajaxContent.innerHTML = body;
    });
});
function keepAlive() {
  var httpRequest = new XMLHttpRequest();
  httpRequest.open('GET', "emner_ajax.php?keepalive=true");
  httpRequest.send(null);
}

setInterval(keepAlive, 840000);

