const emneadministrasjon = {
  initialize: function(){

    this.fillLists()
      .then(this.setEventListeners())
      .finally(console.log('Module loaded'))

  },
  emneliste: {},
  fagpersonliste: {},
  emnetypeliste: {},
  faggruppeliste: {},
  setEventListeners: async function(){
    const that = this;
    const faggruppeselector = document.querySelector('#nyFaggruppe');
    const resetbutton = document.querySelector('#resetNyttEmne');
    const allBtn = document.querySelectorAll('.btn');

    //Hent emneansvarlig fra NTNU
    const btnHentEmneansvarlig = document.querySelector('#hentEmneansvarlig');
    btnHentEmneansvarlig.addEventListener('click',(e)=>{
      console.log('Hent fra NTNU triggered');
      const emnekode = document.querySelector('#nyEmnekode').value.toUpperCase();
      if(emnekode.length !== 0){
        spinner.classList.add('show');
        this.getEmneansvarligFromNTNU(emnekode)
          .then(result => {
            //console.log(epost);
            let id;
            for(let key of Object.keys(this.fagpersonliste)){
              if (this.fagpersonliste[key].email == result[0]) id = key;
            }
            $('#nyEmneansvarlig').val(id).trigger('change');
            faggruppeselector.value = that.fagpersonliste[id].faggruppe;
            document.querySelector('#nyEmnenavn').value = result[1];
          })
          .then(()=>{
            spinner.classList.remove('show');
          })
          .catch(error =>{
            alert(error);
            spinner.classList.remove('show');
          });
      }
    })

    //Oppdater faggruppe når emneansvarlig settes
    $('#nyEmneansvarlig').on('select2:select', function (e) {
      faggruppeselector.value = that.fagpersonliste[e.target.value].faggruppe;
    });

    //Reset skjema for nytt emne
    resetbutton.addEventListener('click', e =>{
      document.querySelector('#nyEmnekode').value = '';
      document.querySelector('#nyEmnenavn').value = '';
      document.querySelector('#nyEmnetype').value = 'null';
      document.querySelector('#nyFaggruppe').value = 'null';
      $('#nyEmneansvarlig').val('null').trigger('change');
    })

    //Lagre nytt emne
    const lagreNyttEmne = function(){
      const emnekode = document.querySelector('#nyEmnekode').value.toUpperCase();
      const emnenavn = document.querySelector('#nyEmnenavn').value;
      const emnetype = document.querySelector('#nyEmnetype').value;
      const faggruppe = document.querySelector('#nyFaggruppe').value;
      const emneansvarlig = document.querySelector('#nyEmneansvarlig').value;
      // console.log(emnekode, emneansvarlig);
      that.lagreNyttEmne(emnekode, emnenavn, emneansvarlig, emnetype, faggruppe)
        .then(()=>{
          //console.log(that);
          that.insertNewNode(emnekode, emnenavn, emneansvarlig, emnetype, faggruppe);
          document.querySelector('#nyEmnekode').value = '';
          document.querySelector('#nyEmnenavn').value = '';
          document.querySelector('#nyEmnetype').value = 'null';
          document.querySelector('#nyFaggruppe').value = 'null';
          $('#nyEmneansvarlig').val('null').trigger('change');
        })
        .catch(reason => {
          alert('Lagring av nytt emne feilet', reason);
        });
    }

    document.querySelector('#submitNyttEmne').addEventListener('click', ()=>{
      lagreNyttEmne();
    })

    //const allBtn = document.querySelectorAll('.btn');
    allBtn.forEach((button)=>{
      let btnType = button.id.split("-")[0];
      let emnekode = button.id.split("-")[1];
      if(btnType == 'btnDelete'){
        button.addEventListener('click', () => this.slettEmne(emnekode));
      } else if (button.id == 'btnUpdate-all'){
        button.addEventListener('click', ()=>{
          spinner.classList.add('show');
          this.updateAllCourses()
            .then(spinner.classList.remove('show'));
        });
      }else if (btnType == 'btnEdit'){
       button.addEventListener('click', (e)=>{
         this.editCourse(e, emnekode);
       });
      }else if (btnType == 'btnUpdate'){
        button.addEventListener('click', ()=>{
          this.refreshCourse(emnekode);
        });

      }else if (btnType == 'btnSave'){
        button.addEventListener('click', ()=>{
          this.saveChanges(emnekode);
        });

      }
    });
  },

  saveChanges: function(emnekode){
    const btnSave = document.querySelector(`#btnSave-${emnekode}`)
    const btnEdit = document.querySelector(`#btnEdit-${emnekode}`)
    btnSave.classList.add('hidden')
    btnEdit.classList.remove('hidden')
    const emneansvarlig = document.querySelector(`#tempEmneansvarlig-${emnekode}`);
    const faggruppe = document.querySelector(`#tempFaggruppe-${emnekode}`);
    const emnetype = document.querySelector(`#tempEmnetype-${emnekode}`);
    const emnenavn = document.querySelector(`#emnenavn-${emnekode}`);
    const emne = this.emneliste[emnekode];
    emne.emneansvarlig = emneansvarlig.value;
    emne.faggruppe = faggruppe.value;
    emne.type = emnetype.value;
    emne.emnenavn = emnenavn.value;
    this.updateCourse(emnekode)
      .then(()=>{
        btnSave.classList.add('hidden')
        btnEdit.classList.remove('hidden')
        emneansvarlig.previousSibling.classList.remove('hidden');
        emneansvarlig.remove();
        faggruppe.previousSibling.classList.remove('hidden');
        faggruppe.remove();
        emnetype.previousSibling.classList.remove('hidden');
        emnetype.remove();
        emnenavn.setAttribute('readonly', true);

      })

  },

  refreshCourse: function(emnekode){
    this.getEmneansvarligFromNTNU(emnekode)
      .then(result => {
        console.log('Updating ' + emnekode);
        // console.log(this.emneliste[key]);
        // console.log(result[0], result[1]);
        let id;
        for(let key of Object.keys(this.fagpersonliste)){
          if (this.fagpersonliste[key].email == result[0]) id = key;
        }
        this.emneliste[emnekode].emneansvarlige = id;
        this.emneliste[emnekode].faggruppe = this.fagpersonliste[id].faggruppe;
        this.emneliste.emnenavn = result[1];
      })
      .then(()=>{
        this.updateCourse(emnekode);
      })
  },

  updateAllCourses: function(){
    promise = new Promise((resolve, reject) => {
      const length = Object.keys(this.emneliste).length;
      let i = 0;
      for (key of Object.keys(this.emneliste)){
        i++;
        this.getEmneansvarligFromNTNU(key)
          .then(result => {
            console.log('Updating ' + key);
            // console.log(this.emneliste[key]);
            // console.log(result[0], result[1]);
            let id;
            for(let key of Object.keys(this.fagpersonliste)){
              if (this.fagpersonliste[key].email == result[0]) id = key;
            }
            this.emneliste[key].emneansvarlige = id;
            this.emneliste[key].faggruppe = this.fagpersonliste[id].faggruppe;
            this.emneliste.emnenavn = result[1];
          })
          .then(()=>{
            this.updateCourse(key);
          })
        if (i == length) resolve('Success!');
      }

    });
    return promise;
  },

  editCourse: function(e, emnekode){
    let inputEmnenavn = document.querySelector(`#emnenavn-${emnekode}`);
    let inputFaggruppe = document.querySelector(`#faggruppe-${emnekode}`);
    let inputEmneansvarlig = document.querySelector(`#emneansvarlig-${emnekode}`);
    let inputEmnetype = document.querySelector(`#emnetype-${emnekode}`);
    e.currentTarget.classList.add('hidden');
    for(input of [inputEmnetype, inputEmneansvarlig, inputFaggruppe]){
      input.classList.add('hidden');
    }
    inputEmnenavn.removeAttribute('readonly');
    const emneansvarligSelector = document.createElement('select');
    for (let key of Object.keys(this.fagpersonliste)){
      const option = document.createElement('option');
      option.value = key;
      option.text = `${this.fagpersonliste[key].navn}, ${this.fagpersonliste[key].fornavn}`;
      if(key == this.emneliste[emnekode].emneansvarlig) option.selected = true;
      emneansvarligSelector.appendChild(option)
    }
    emneansvarligSelector.id = `tempEmneansvarlig-${emnekode}`;
    inputEmneansvarlig.insertAdjacentElement('afterend', emneansvarligSelector);

    const emnetypeSelector = document.createElement('select');
    for(let key of Object.keys(this.emnetypeliste)){
      const option = document.createElement('option');
      option.value = key;
      option.text = this.emnetypeliste[key].type;
      if (key == this.emneliste[emnekode].type) option.selected;
      emnetypeSelector.add(option);
    }
    emnetypeSelector.id = `tempEmnetype-${emnekode}`;
    inputEmnetype.insertAdjacentElement('afterend', emnetypeSelector);

    const faggruppeselector = document.createElement('select');
    for(let key of Object.keys(this.faggruppeliste)){
      const option = document.createElement('option');
      option.value = key;
      option.text = this.faggruppeliste[key].forkortelse;
      if(key == this.emneliste[emnekode].faggruppe) option.selected = true;
      faggruppeselector.add(option);
    }
    faggruppeselector.id = `tempFaggruppe-${emnekode}`;
    inputFaggruppe.insertAdjacentElement('afterend', faggruppeselector);
    document.querySelector(`#btnSave-${emnekode}`).classList.remove('hidden');
  },

  updateCourse: function (emnekode){
    // let inputEmnekode = document.querySelector(`#emnekode-${emnekode}`);
    let inputEmnenavn = document.querySelector(`#emnenavn-${emnekode}`);
    let inputFaggruppe = document.querySelector(`#faggruppe-${emnekode}`);
    let inputEmneansvarlig = document.querySelector(`#emneansvarlig-${emnekode}`);
    let inputEmnetype = document.querySelector(`#emnetype-${emnekode}`);
    let emne = this.emneliste[emnekode];
    let emneansvarlig = emne.emneansvarlig;
    let emnenavn = emne.emnenavn;
    let emnetype = emne.type;
    let faggruppe = emne.faggruppe;
    const post = new FormData;

    post.append('emnekode', emnekode);
    post.append('emnenavn', emnenavn);
    post.append('emnetype', emnetype);
    post.append('faggruppe', faggruppe);
    post.append('emneansvarlig', emneansvarlig);
    const promise = new Promise((resolve, reject) => {
      fetch('general_ajax.php?updateCourse=' + emnekode, {
        method: 'POST',
        body: post
      })
        .then(result => result.text())
        .then(()=>{
          inputEmnenavn.value = emnenavn;
          inputEmneansvarlig.value = `${this.fagpersonliste[emneansvarlig].navn}, ${this.fagpersonliste[emneansvarlig].fornavn}`;
          inputEmnetype.value = this.emnetypeliste[emnetype].type;
          inputFaggruppe.value = this.faggruppeliste[faggruppe].forkortelse;
        })
        .then(() => {
          for(let input of [inputEmnenavn, inputEmnetype, inputEmneansvarlig, inputFaggruppe]){
            const parent = input.parentNode;
            parent.classList.remove('changed');
            parent.classList.add('lagret');
            resolve('Success!');
            setTimeout(function(){parent.classList.remove('lagret')}, 3000);
          }
        })
    })
    return promise;
  },

  slettEmne: function(emnekode){
    if(confirm('NB! Sletting av emner kan i enkelte tilfeller føre til problemer. Emner bør kun slettes dersom du ved en feil har opprettet et emne med feil emnekode. For utgåtte emner bytter du emnetype til "Deaktivert"')){
      if(confirm('Er du HELT sikker på at emnet skal slettes')){
        console.log('Deleting course ' + emnekode);
        fetch('general_ajax.php?slettEmne=' + emnekode)
          .then(()=>{
            const parent = document.querySelector('#rowEmne-' + emnekode).parentNode;
            const rowEmne = document.querySelector('#rowEmne-' + emnekode);
            parent.removeChild(rowEmne);
          })
          .then(()=>true)
          .catch(error => alert(alert));
      }
    }
  },

  lagreNyttEmne: function(emnekode, emnenavn, emneansvarlig, emnetype, faggruppe){
    const formData = new FormData();
    formData.append('emnenavn', emnenavn);
    formData.append('emneansvarlig', emneansvarlig);
    formData.append('emnekode', emnekode);
    formData.append('emnetype', emnetype);
    formData.append('faggruppe', faggruppe);
    const promise = new Promise((resolve, reject) => {
      fetch('general_ajax.php?nyttEmne',{
        method: 'POST',
        body: formData
      })
        .then(result => result.text())
        .then((data) => {
          console.log(data);
          resolve('Success!', data);
        })
        .catch(error => reject('failure', error))
    });
    return promise;
  },

  insertNewNode: function(emnekode, emnenavn, emneansvarlig, emnetype, faggruppe){
    const template = document.querySelector('#emnerow-template');
    const newRow = template.cloneNode(true);
    newRow.classList.remove('hidden');
    const colEmnekode = newRow.querySelector('#templateEmnekode');
    const colEmnenavn = newRow.querySelector('#templateEmnenavn');
    const colEmneansvarlig = newRow.querySelector('#templateEmneansvarlig');
    const colEmnetype = newRow.querySelector('#templateEmnetype');
    const colFaggruppe = newRow.querySelector('#templateFaggruppe');
    colEmnekode.id = `emnekode-${emnekode}`;
    colEmnekode.value = emnekode;
    colEmnenavn.id = `emnenavn-${emnekode}`;
    colEmnenavn.value = emnenavn;
    colEmneansvarlig.id = `emneansvarlig-${emnekode}`;
    colEmneansvarlig.value = `${this.fagpersonliste[emneansvarlig].navn}, ${this.fagpersonliste[emneansvarlig].fornavn}`;
    colEmnetype.id = `emnetype-${emnekode}`;
    colEmnetype.value = this.emnetypeliste[emnetype].type;
    colFaggruppe.id = `faggruppe-${emnekode}`;
    colFaggruppe.value = this.faggruppeliste[faggruppe].forkortelse;
    //console.log(newRow);
    //document.querySelector('#emnelisteliste').insertBefore(newRow, document.querySelector('#emneHeading').nextElementSibling);
    newRow.classList.add('lagret');
    newRow.id = `rowEmne-${emnekode}`;
    setTimeout(function (){
      newRow.classList.remove('lagret');}, 3000);
    document.querySelector('#emneHeading').insertAdjacentElement('afterend', newRow);


  },

  emnelisteCreate: function(){
    promise = new Promise(resolve =>{
      fetch('general_ajax.php?getEmnelisteAlle=all')
        .then(response => response.json())
        .then(data =>{
          for(let emne of data){
            //console.log(this.emneliste);
            //this.emneliste[emne.emnekode] = {};
            this.emneliste[emne.emnekode] = emne;
          }
        })
        .then(resolve('Success!'))
    })
    return promise;
  },
  fagpersonlisteCreate: function(){
    promise = new Promise(resolve => {
      fetch('general_ajax.php?getBrukereAktive')
        .then(response => response.json())
        .then(data =>{
          for(let fagperson of data){
            this.fagpersonliste[fagperson.id] = fagperson;
          }
        })
        .then(resolve('Success!'))
    })
    return promise;
  },

  emnetypelisteCreate: function(){
    promise = new Promise(resolve => {
      fetch('general_ajax.php?emnetyper')
        .then(response => response.json())
        .then(data =>{
          for(let emnetype of data){
            this.emnetypeliste[emnetype.id] = emnetype;
          }
        })
        .then(resolve('Success!'))
    })
    return promise;
  },
  faggruppelisteCreate: function(){
    promise = new Promise(resolve => {
      fetch('general_ajax.php?getFaggruppeAktive')
        .then(response => response.json())
        .then(data =>{
          for(let faggruppe of data){
            this.faggruppeliste[faggruppe.id] = faggruppe;
          }
        })
        .then(resolve('Success!'))
    })
    return promise;
  },

  fillLists: function(){
    promise = new Promise(resolve => {
      this.fagpersonlisteCreate()
        .then(()=>{
          this.emnelisteCreate()
            .then(()=>{
              this.faggruppelisteCreate();
            })
            .then(()=>{
              this.emnetypelisteCreate()
                .then(resolve('Success!'))
            })
        })
    })
    return promise;
  },

  setEmneansvarlig: function(emnekode, emneansvarligEpost){
    //let that = this;
    promise = new Promise(resolve => {
      fetch(`general_ajax.php?setEmneansvarlig=true&emnekode=${emnekode}&emneansvarlig=${emneansvarligEpost}`)
        .then(function(response){
          return response.json();
        })
        .then(function(json) {
          // that.emneListe[emnekode].emneansvarlig = json.id;
          resolve(json.id);
        })
    })
    return promise;

  },
  getEmneansvarligFromNTNU: function (emnekode) {
    const url = `https://www.ntnu.no/studier/emner/${emnekode}#tab=omEmnet`;
    let doc = undefined;
    // const that = this;
    promise = new Promise((resolve, reject) => {
      fetch(`general_ajax.php?getURL=${url}`)
        .then(function(response){
          return response.text();
        })
        .then(function(text){
          doc = new DOMParser().parseFromString(text, 'text/html');
          const div = doc.getElementById('omEmnet');
          let emnetittel;
          if(!div){
            reject('Could not find course info for current academic year');
          }else{
            const title = doc.querySelector('head > title');
            emnetittel = title.innerHTML.split(" - ")[1];
            console.log(emnetittel);
            const spans = div.querySelectorAll('span');
            let spanEmneansvarlig;
            for(let span of spans){
              if (span.innerText === 'Emneansvarlig/koordinator:'){
                spanEmneansvarlig = span;
                break;
              }
            }
            const ul = spanEmneansvarlig.nextElementSibling;
            const a = ul.querySelector('a');
            // if(that.emneListe[emnekode] === undefined) that.emneListe[emnekode] = {};
            let emneansvarligURL = a.href
            fetch(`general_ajax.php?getURL=${emneansvarligURL}`)
              .then(function(response){
                return response.text();
              })
              .then(function(text){
                doc = new DOMParser().parseFromString(text, 'text/html');
                // that.emneListe[emnekode].emneansvarligEpost = doc.querySelector("#main-profile span div div div.coreinfo a span").innerText;
                // that.setEmneansvarlig(emnekode, that.emneListe[emnekode].emneansvarligEpost);
                let emneansvarligEpost = doc.querySelector("#main-profile span div div div.coreinfo a span").innerText;
                //console.log(emnetittel);
                resolve([emneansvarligEpost, emnetittel]);
              });
          }

        })
        .catch(error => reject(error));
    })
    return promise;
  },
}
