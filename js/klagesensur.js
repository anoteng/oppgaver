const sensorliste = {
  emneListe: {},
  faggrupper: {},
  emneansvarlige: {},
  nodeList: [],
  eksterneSensorer: [],
  interne: {},
  eksterne: {},

  initialize: function(){
    console.log('Initializing');
    this.courselist()
      .then(this.fillEmneansvarligList()
        .then(this.fillFaggruppeList()
          .then(()=>{
            this.createNodes();
            this.fillEksternSensorListe();
          })
        )
      );
    const emneSelector = document.querySelector('#emnekodeSelector');
    const faggruppeSelector = document.querySelector('#faggruppeSelector');
    const emneansvarligSelector = document.querySelector('#emneansvarligSelector');
    const selectors = [emneSelector, faggruppeSelector, emneansvarligSelector];
    faggruppeSelector.addEventListener('change', (e) => this.filterUpdate(e))
    let that = this;
    $('#emneansvarligSelector').on('select2:select', function (e) {
      that.filterUpdate(e);
    });
    $('#emnekodeSelector').on('select2:select', function (e) {
      that.filterUpdate(e);
    });
    document.querySelector('#nullstill').addEventListener('click', function (){
      faggruppeSelector.selectedIndex = 'null';
      $('#emneansvarligSelector').val('null').trigger('change');
      $('#emnekodeSelector').val('null').trigger('change');
      for(select of selectors){
        select.dispatchEvent(new Event('change'));
      }
    })

  },
  createNodes: function(){
    function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }
    sleep(1000).then(()=>{
      console.log('Creating nodes');
      const template = document.querySelector('#sensorRowTemplate');
      const placeholder = document.querySelector('#sensorRowsPlaceholder');
      placeholder.innerHTML = '';
      //console.log(this);
      //console.dir(Object.keys(this.emneListe));
      for (let key of Object.keys(this.emneListe)) {

        const emne = this.emneListe[key]
        console.log('Creating row ' + emne.emnekode);
        const newRow = template.cloneNode(true);
        newRow.id = `${emne.emnekode}-sensorRow`;
        newRow.querySelector('.sensorColEmnekode').innerHTML = emne.emnekode;
        newRow.querySelector('.sensorColEmneansvarlig').innerHTML = this.emneansvarlige[emne.emneansvarlig].navn + ', ' + this.emneansvarlige[emne.emneansvarlig].fornavn;
        const sensorColIntern = newRow.querySelector('.sensorColIntern');
        const sensorColEkstern = newRow.querySelector('.sensorColEkstern');
        const sensorColEdit = newRow.querySelector('.sensorColEdit');
        newRow.classList.remove('sensorRowTemplate');
        this.getEkstern(emne.emnekode)
          .then(() =>{
            this.getIntern(emne.emnekode)
              .then(() => {
                let eksterne = Object.keys(this.eksterne[emne.emnekode]).length !== 0 ? this.eksterne[emne.emnekode] : [];
                let interne = Object.keys(this.interne[emne.emnekode]).length !== 0 ? this.interne[emne.emnekode] : [];

                this.nodeList.push({emne: emne.emnekode, node: newRow, visible: true, externals: eksterne, internals: interne});
                for (let sensor of eksterne){
                  const ol = sensorColEkstern.querySelector('ol');
                  const li = document.createElement('li');
                  li.value = sensor.sensor;
                  li.innerHTML = `<a href="mailto:${this.eksterneSensorer[sensor.sensor].email}">${this.eksterneSensorer[sensor.sensor].navn}, ${this.eksterneSensorer[sensor.sensor].fornavn}</a>`;
                  const deleteBtn = this.deleteButton();
                  li.appendChild(deleteBtn);
                  deleteBtn.addEventListener('click', this.deleteSensorEkstern);
                  ol.appendChild(li);
                  const hiddenInput = document.createElement('input');
                  hiddenInput.classList.add('hidden');
                  hiddenInput.value = emne.emnekode;
                  ol.appendChild(hiddenInput);
                }
                //console.dir(interne);
                for (let sensor of interne){
                  const ol = sensorColIntern.querySelector('ol');
                  const li = document.createElement('li');
                  //console.dir(sensor);
                  li.value = sensor.sensor;
                  li.innerHTML = `<a href="mailto:${this.emneansvarlige[sensor.sensor].email}">${this.emneansvarlige[sensor.sensor].navn}, ${this.emneansvarlige[sensor.sensor].fornavn}</a>`;
                  const deleteBtn = this.deleteButton();
                  li.appendChild(deleteBtn);
                  deleteBtn.addEventListener('click', this.deleteSensorIntern);
                  ol.appendChild(li);
                  const hiddenInput = document.createElement('input');
                  hiddenInput.classList.add('hidden');
                  hiddenInput.value = emne.emnekode;
                  ol.appendChild(hiddenInput);
                }
                this.btnAddExternal(sensorColEkstern, emne.emnekode);
                this.btnAddInternal(sensorColIntern, emne.emnekode);
                placeholder.appendChild(newRow);

              })
              // .then(()=>{
              //     $(document).ready(function() {
              //       $('.js-example-basic-single').select2();
              //     })
              //
              // })
          })

        let element = document.querySelector('#sensorRowsPlaceholder');
        sortHTML(element);
      }
    })
  },

  deleteSensorEkstern: function (e){
    //console.log(e.currentTarget);
    if(confirm('Er du sikker på at du vil slette denne sensoren fra emnet?')){
      const li = e.currentTarget.parentElement;
      const ol = li.parentElement;
      const sensor = li.value;
      const emnekode = ol.querySelector('input').value;
      //console.log(li, ol, sensor);
      fetch(`general_ajax.php?deleteSensor=${sensor}&emnekode=${emnekode}&intext=ext`)
        .then(()=>{
          ol.removeChild(li);
        })
    }
  },
  deleteSensorIntern: function (e){
    if(confirm('Er du sikker på at du vil slette denne sensoren fra emnet?')){
      const li = e.currentTarget.parentElement;
      const ol = li.parentElement;
      const sensor = li.value;
      const emnekode = ol.querySelector('input').value;
      //console.log(li, ol, sensor);
      fetch(`general_ajax.php?deleteSensor=${sensor}&emnekode=${emnekode}&intext=int`)
        .then(()=>{
          ol.removeChild(li);
        })
    }
  },

  orderNodes: function(){
    let element = document.querySelector('#sensorRowsPlaceholder');
    sortHTML(element);
  },

  editCourse: function (emnekode){
    const overlay = document.getElementById("overlay");
    overlay.style.display = "block";
  },

  btnAddExternal: function(element, emnekode){
    const select = document.createElement("select");
    select.classList.add('js-example-basic-single');
    select.id = 'external-' + emnekode;
    let option = document.createElement('option')
    option.selected = true;
    option.value = 'none';
    option.text = "";
    select.add(option);
    for (let key in this.eksterneSensorer){
      option = document.createElement('option');

      let sensor = this.eksterneSensorer[key];

      option.value = sensor.id;
      option.text = `${sensor.navn}, ${sensor.fornavn}`;
      select.add(option);
    }
    sortHTML(select)
    select.value = 'none'
    element.appendChild(select);
    select.addEventListener('change',(e) =>{
      const ol = element.querySelector('ol');
      const li = document.createElement('li');
      li.value = e.currentTarget.value;
      li.innerHTML = `<a href="mailto:${this.eksterneSensorer[e.currentTarget.value].email}">${this.eksterneSensorer[e.currentTarget.value].navn}, ${this.eksterneSensorer[e.currentTarget.value].fornavn}</a>`;

      fetch('general_ajax.php?setSensorEkstern=' + e.currentTarget.value + '&emnekode=' + emnekode)
        .then(()=>{
          ol.appendChild(li);
        })
    })
  },
  btnAddInternal: function(element, emnekode){
    const select = document.createElement("select");
    let option = document.createElement('option')
    select.classList.add('js-example-basic-single')
    option.selected = true;
    option.value = 'none';
    option.text = "";
    select.add(option);
    for (let key in this.emneansvarlige){
      option = document.createElement('option');

      let sensor = this.emneansvarlige[key];

      option.value = sensor.id;
      option.text = `${sensor.navn}, ${sensor.fornavn}`;
      select.add(option);
    }
    sortHTML(select)
    select.value = 'none'
    element.appendChild(select);
    select.addEventListener('change',(e) =>{
      const ol = element.querySelector('ol');
      const li = document.createElement('li');
      li.value = e.currentTarget.value;
      li.innerHTML = `<a href="mailto:${this.emneansvarlige[e.currentTarget.value].email}">${this.emneansvarlige[e.currentTarget.value].navn}, ${this.emneansvarlige[e.currentTarget.value].fornavn}</a>`;

      fetch('general_ajax.php?setSensorIntern=' + e.currentTarget.value + '&emnekode=' + emnekode)
        .then(()=>{
          ol.appendChild(li);
        })
    })
  },

  fillEksternSensorListe: function(){
    promise = new Promise((resolve) =>{
      fetch('general_ajax.php?getAlleEksternSensor=true')
        .then(result => result.json())
        .then(data => {
          for(let sensor of data){
            this.eksterneSensorer[sensor.id] = sensor;
          }
        })
        .then(resolve('Success!'))
    })
    return promise;
  },

  displayNodes: function(node){
    const placeholder = document.querySelector('#sensorRowsPlaceholder');
    placeholder.innerHTML = '';
    placeholder.appendChild(node)
  },

  filterUpdate: function(e){
    //oppdater emneliste
    const emneSelector = document.querySelector('#emnekodeSelector');
    const faggruppeSelector = document.querySelector('#faggruppeSelector');
    const emneansvarligSelector = document.querySelector('#emneansvarligSelector');


    if(e.currentTarget.id === emneSelector.id) {

      for(let key of this.nodeList){
        console.log(key);
        if(key.emne === emneSelector.value){
          key.node.classList.remove('hidden');
        }else if(emneSelector.value === "none"){
          key.node.classList.remove('hidden');
        }else{
          key.node.classList.add('hidden');
        }
      }
    }
    if(e.currentTarget.id === emneansvarligSelector.id){
      for(let key of this.nodeList){
        if(emneansvarligSelector.value == this.emneListe[key.emne].emneansvarlig){
          key.node.classList.remove('hidden');
        }else if(emneansvarligSelector.value === "none"){
          key.node.classList.remove('hidden');
        }else{
          key.node.classList.add('hidden');
        }
      }
    }
    if(e.currentTarget.id === faggruppeSelector.id){
      for(key of this.nodeList){
        console.log(faggruppeSelector.value, this.emneListe[key.emne].faggruppe, faggruppeSelector.value === this.emneListe[key.emne].faggruppe);
        if(faggruppeSelector.value == this.emneListe[key.emne].faggruppe) {

          key.node.classList.remove('hidden')
        }else if(faggruppeSelector.value === "none"){
          key.node.classList.remove('hidden')
        }else{
          key.node.classList.add('hidden')
        }
      }
    }
  },

  fillFaggruppeList: function() {
    promise = new Promise(resolve => {
    fetch('general_ajax.php?getFaggruppeAktive')
      .then(result => result.json())
      .then(data => {
        for(let faggruppe of data){

          this.faggrupper[faggruppe.id.toString()] = faggruppe;
        }
        const faggruppeSelector = document.querySelector('#faggruppeSelector');
        for (let key of Object.keys(this.faggrupper)){
          const faggruppe = this.faggrupper[key];
          const option = document.createElement('option');
          option.value = faggruppe.id;
          option.innerText = `${faggruppe.forkortelse} - ${faggruppe.navn}`;
          faggruppeSelector.append(option)
        }
      })
      .then(resolve("Success!"))
  })
  return promise;
},

  fillEmneansvarligList: function(){
    promise = new Promise((resolve) => {
      fetch('general_ajax.php?getBrukereAktive')
        .then(result => result.json())
        .then(data => {
          const emneansvarligSelector = document.querySelector('#emneansvarligSelector');

          for (let emneansvarlig of data) {

            const id = emneansvarlig.id.toString();
            //console.dir(id);
            this.emneansvarlige[id] = emneansvarlig;
            //console.log(this.emneansvarlige[id]);
            const option = document.createElement('option');
            option.value = emneansvarlig.id;
            option.innerText = `${emneansvarlig.navn}, ${emneansvarlig.fornavn}`;
            emneansvarligSelector.append(option)
          }
        })
        .then(()=>{
          $(document).ready(function() {
            $('.js-example-basic-single').select2();
          });
        })
        .then(() => {
          resolve("Success!");
        })
    })
    return promise;
  },

  courselist: function(){
    promise = new Promise((resolve, reject) => {
      fetch('general_ajax.php?getEmnelisteAlle')
        .then(result => result.json())
        .then(async data => {
          for(let emne of data){
            this.emneListe[emne.emnekode] = emne;
            // if(emne.emneansvarlig == null){
            //   await this.getEmneansvarligFromNTNU(emne.emnekode);
            // }
          }
          const emneSelector = document.querySelector('#emnekodeSelector');
          for (let key of Object.keys(this.emneListe)){
            const emne = this.emneListe[key];
            const option = document.createElement('option');
            option.value = emne.emnekode;
            option.innerText = `${emne.emnekode} - ${emne.emnenavn}`;
            emneSelector.append(option)
          }
        })
        .then(() => {
          this.fillEmneansvarligList();
        })
        .then(resolve("Success!"))
    })
    return promise;
  },

  getEkstern: function(emne){
    promise = new Promise(resolve => {
      fetch('general_ajax.php?getSensorEkstern=' + emne)
        .then(result => result.json())
        .then(data => {
          this.eksterne[emne] = data;
        })
        .then(()=>{
          resolve('Success!');
        })
    })
    return promise;
  },
  getIntern: function(emne){
      promise = new Promise(resolve => {
        fetch('general_ajax.php?getSensorIntern=' + emne)
          .then(result => result.json())
          .then(data => {
            this.interne[emne] = [];
            for(let sensor of data){
              this.interne[emne].push(sensor);
            }

          })
          .then(()=>{
            resolve('Success!');
          })
    })
    return promise;
  },
  deleteButton: function (){
    const deleteBtn = document.createElement('button');
    const deleteSpan = document.createElement('span');
    const deleteImage = document.createElement("img");
    deleteImage.src = 'img/delete.png';
    deleteImage.style.width = '20px';
    deleteSpan.appendChild(deleteImage);
    deleteBtn.appendChild(deleteImage)
    deleteBtn.classList.add('btn');
    return deleteBtn;
  }
}
