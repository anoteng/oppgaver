function fillReport(result){
  //console.log("debug");
  let table = document.getElementById('studentListeTbody');
  table.innerHTML = '';
  let i;
  if(result.length !== 0) {
    for (i = 0; i < result[0].length; i++) {
      let row = table.insertRow();
      let cell = row.insertCell();
      cell.innerHTML = `<a href="#" onclick="$('#ajax-content').load('emne.php?id=${result[0][i]['emne']}&amp;terminid=${result[0][i]['termin']}&amp;vurdid=${result[0][i]['vurdid']}>');return false;">${result[0][i]['emne']}</a>`;
      cell = row.insertCell();
      cell.innerHTML = `${result[0][i]['year']}-${result[0][i]['month']}`;
      cell = row.insertCell();
      cell.innerHTML = result[0][i]['navn'];
      cell = row.insertCell();
      cell.innerHTML = result[0][i]['epost'];
      cell = row.insertCell();
      cell.innerHTML = result[0][i]['veileder'];
      cell = row.insertCell();
      cell.innerHTML = result[0][i]['sensor1'];
      cell = row.insertCell();
      cell.innerHTML = result[0][i]['sensor2'];
    }
  }
}

function hentRapport(id){
  let rapportType = document.getElementById("filterFagperson").value;
  let termin = document.getElementById("ddlTermin").value;
  //console.log(`rapportType: ${rapportType} - ${id}\n termin: ${termin}`);
  let xmlhttp = new XMLHttpRequest();

  switch (rapportType) {

    case "veileder":
      xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
          let response = JSON.parse(this.responseText);
          // console.log(response);
          fillReport(response);
        }
      };
      xmlhttp.abort();
      xmlhttp.open("GET", `rapporter_ajax.php?veileder=${id}&termin=${termin}`, true);
      xmlhttp.send();
      break;
    case "sensor":
      xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
          let response = JSON.parse(this.responseText);
          fillReport(response);
        }
      };
      xmlhttp.abort();
      xmlhttp.open("GET", `rapporter_ajax.php?sensor=${id}&termin=${termin}`, true);
      xmlhttp.send();
      break;
  }

}
