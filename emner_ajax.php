<?php
include("config.php");
include("classes.php");
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION['access'])){
  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_level();
if($access->access_level != 1 AND $access->access_level != 2) {
  header("Location: index.php");
}
if(isset($_GET['keepalive'])){
	die();
}
if(isset($_GET['hentsensor'])){
  $vurderingsenhet = new vurderingsenhet();
  if($_GET['hentsensor'] == "intern"){
    $vurderingsenhet->id = $_GET['vurderingsenhet'];
    $vurderingsenhet->list_brukere();
    echo json_encode($vurderingsenhet->brukerliste);
  }else{
    $vurderingsenhet->list_eksterne();
    echo json_encode($vurderingsenhet->ekstern_sensor);
  }
}
if(isset($_GET['hentveileder'])){
  $vurderingsenhet = new vurderingsenhet();
  if($_GET['hentveileder'] == "intern"){
    $vurderingsenhet->id = $_GET['vurderingsenhet'];
    $vurderingsenhet->list_interne_brukere();
    echo json_encode($vurderingsenhet->brukerliste);
  }else{
    $vurderingsenhet->list_brukere();
    echo json_encode($vurderingsenhet->brukerliste);
  }
}
