<?php
include('config.php');
include('classes.php');
if(isset($_GET['logout'])){
  $access = new access();
  $access->logout();
  session_destroy();
  die('Session destroyed, token deleted!');
}
if(isset($_GET['email'])){
  $access = new access();
  $access->email_address = $_GET['email'];
  $token_result = $access->send_login();
}
if(isset($_GET['token'])){
  $_SESSION['access'] = new access;
  $_SESSION['access']->token = $_GET['token'];
  if($_SESSION['access']->check_access_token()){
    header('location: index.php');
  }

}
?>
<!doctype html>
<html class="no-js" lang="no">

<head>
  <meta charset="utf-8">
  <title>OppgaveWeb login</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.css">

  <meta name="theme-color" content="#fafafa">
</head>

<body>
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->
  <script src="js/vendor/modernizr-3.8.0.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>
  <?php
  if(isset($_SESSION['access'])){
    echo '<p>Innlogget som bruker ' . $_SESSION['access']->user_name . '. <a href="?logout=true">Logge ut?</a><br />'.
      'NB! Om du logger ut slettes sesjonen din og du må be om ny lenke for å logge inn igjen</p>'.
      '<p><strong><a href="index.php">Klikk her for å komme til forsiden</a></strong></p>';
  }
  if(isset($token_result)){
    switch ($token_result){
      case true:
        echo '<p>En innloggingslenke blir sendt til din epostadresse. Lenken vil fungere fram til '. date('Y-m-d H:i:s', time() + TOKEN_VALIDITY . '.</p>');
        break;

      case false:
        echo '<p>Det ser ut til at du har sendt oss en epostadresse som ikke er registrert. Ta kontakt med studieteamet på <a href="mailto:studier@ibm.ntnu.no?subject=Tilgang til OppgaveWeb">studier@ibm.ntnu.no</a>';
        break;
    }
  }
  ?>
  <form action="login.php" method="get">
    <label for="email">Logg inn med epostadresse:</label>
    <input type="email" id="email" name="email" />
    <input type="submit">
  </form>
</body>

</html>
