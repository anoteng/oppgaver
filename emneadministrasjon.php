<?php
include('config.php');
include('classes.php');
?>
<div class="container-fluid">
  <div class="filters">
    <div class="row">
      <h3>Legg til nytt emne</h3>
    </div>
    <div class="row">
      <div class="col-auto"><label>Emnekode: <br><input type="text" style="text-transform: uppercase;" name="emnekode" id="nyEmnekode"></label></div>
      <div class="col-auto"><label>Emnenavn: <br><input type="text" name="emnenavn" id="nyEmnenavn"></label></div>
      <div class="col-auto"><label>Emneansvarlig: <br><select class="js-example-basic-single" name="emneansvarlig" id="nyEmneansvarlig">
            <option value="null" selected>-</option>
            <?php
            $sensor = new klagesensur();
            $fagpersoner = $sensor->getAllEmneansvarlig();
            foreach ($fagpersoner as $row){
              echo "<option value='$row[id]'>$row[navn], $row[fornavn]</option>";
            }
            ?>
          </select></label></div>
      <div class="col-auto"><label>Emnetype: <br><select id="nyEmnetype">
            <option value="null" selected>-</option>
            <?php
            $sensor = new klagesensur();
            $emnetyper = $sensor->getEmnetyper();
            foreach ($emnetyper as $row){
              echo "<option value='$row[id]'>$row[type]</option>";
            }
            ?>
          </select></label></div>
      <div class="col-auto"><label>Faggruppe: <br><select name="faggruppe" id="nyFaggruppe">
                <option value="null" selected>-</option>
                <?php
                $sensor = new klagesensur();
                $faggrupper = $sensor->getAllFaggruppe();
                foreach ($faggrupper as $row){
                  echo "<option value='$row[id]'>$row[forkortelse] - $row[navn]</option>";
                }
                ?>
              </select></label></div>
      <div class="col-auto"><button id="submitNyttEmne">Lagre</button> <button id="resetNyttEmne">Reset</button> <button id="hentEmneansvarlig" title="Hent emneinformasjon fra NTNUs nettsider">Hent emneinfo</button></div>
<!--      <div class="col"></div>-->
    </div>
  </div>
  <div id="emnelisteliste" class="filters">
    <h3>Eksisterende emner</h3>
    <div id="emneHeading" class="row heading">
      <div class="col-1">Emnekode</div>
      <div class="col-2">Emnenavn</div>
      <div class="col-1">Faggruppe</div>
      <div class="col-2">Emneansvarlig</div>
      <div class="col-2">Emnetype</div>
      <div class="col">
        </button><button class='btn' id='btnUpdate-all' title='Oppdater emneinformasjon fra NTNUs nettsider'>
          <img src='img/search.png' style='width: 20px'>
        </button>
      </div>

    </div>
    <?php
      $emneadmin = new emneadministrasjon();
      $emner = $emneadmin->getAllCourses(1);

      $faggrupper = $emneadmin->faggrupper($faggrupper);
      $fagpersoner = $emneadmin->emneansvarlige($fagpersoner);
      $emnetyper = $emneadmin->emnetyper($emnetyper);
      foreach ($emner as $emne){
        echo "
        <div class='row rowhover' id='rowEmne-$emne[emnekode]'>
          <div class='col-1'><input type='text' readonly id='emnekode-$emne[emnekode]' value='$emne[emnekode]'></div>
          <div class='col-2'><input type='text' readonly id='emnenavn-$emne[emnekode]' value='$emne[emnenavn]' size='40'></div>
          <div class='col-1'><input type='text' readonly id='faggruppe-$emne[emnekode]' value='{$faggrupper[$emne['faggruppe']]['forkortelse']}' size='5'></div>
          <div class='col-2'><input type='text' readonly id='emneansvarlig-$emne[emnekode]' value='{$fagpersoner[$emne['emneansvarlig']]['navn']}, {$fagpersoner[$emne['emneansvarlig']]['fornavn']}' size='30'></div>
          <div class='col-2'><input type='text' readonly id='emnetype-$emne[emnekode]' value='{$emnetyper[$emne['type']]['type']}'></div>
          <div class='col'>
              <button class='btn' id='btnEdit-$emne[emnekode]' title='Rediger emne'>
                  <img src='img/edit.png' style='width: 20px'>
              </button>
              <button class='btn hidden' id='btnSave-$emne[emnekode]' title='Lagre endringer'>
                  <img src='img/save.png' style='width: 20px'>
              </button><button class='btn' id='btnDelete-$emne[emnekode]' title='Slett emne'>
                  <img src='img/delete.png' style='width: 20px'>
              </button>
              </button><button class='btn' id='btnUpdate-$emne[emnekode]' title='Oppdater emneansvarlig fra NTNUs nettsider'>
                  <img src='img/search.png' style='width: 20px'>
              </button>
          </div>
        </div>
        ";
      }
    ?>
    <div id="emnerow-template" class="row rowhower hidden">
      <div class='col-1'><input type='text' readonly id="templateEmnekode"></div>
      <div class='col-2'><input type='text' readonly id="templateEmnenavn" size="40"></div>
      <div class='col-1'><input type='text' readonly id="templateFaggruppe" size="5"></div>
      <div class='col-2'><input type='text' readonly id="templateEmneansvarlig" size="30"></div>
      <div class='col-2'><input type='text' readonly id="templateEmnetype"></div>
      <div class='col'>
        <button class='btn' id="templateBtnEdit">
          <img src='img/edit.png' style='width: 20px'>
        </button>
        <button class='btn hidden' id="templateBtnSave">
          <img src='img/save.png' style='width: 20px'>
        </button><button class='btn' id="templateBtnDelete">
          <img src='img/delete.png' style='width: 20px'>
        </button>
      </div>
    </div>
  </div>
</div>
