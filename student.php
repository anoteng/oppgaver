<?php
include("config.php");
include("classes.php");
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION['access'])){
  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_level();
if($access->access_level != 1 AND $access->access_level != 2){
  header("Location: index.php");
}
?>
<script src="js/student.js"></script>
  <p><h2>Denne funksjonen er under utvikling</h2></p>
<table id="tblStudentinfo">
  <thead id="tblHeadStudentinfo">
  <tr>
    <th id="studNavn">
    </th>
    <th id="studEpost"></th>
  </tr>
  <tr>
    <td id="tdVurdmelding">
      <select id="ddlVurdmelding" onchange="studTblBody(<?php echo $_GET['studentid'] ?>)"></select>
    </td>
  </tr>
  </thead>
  <tbody id="tblBodyStudentinfo">

  </tbody>
</table>
<?php
if(isset($_GET['studentid'])){
  echo "<script>fillStudentTable({$_GET['studentid']})</script>";
  echo "<script>studTblBody(document.getElementById('ddlVurdmelding').value, {$_GET['studentid']})</script>";
}
