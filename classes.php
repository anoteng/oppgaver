<?php
session_start();
class sql{
  public $con;
  public $resultArray = [];

  function __construct(){
    $this->con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$this->con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $this->con->connect_error);
    }
  }

  public function selectQuery($sql, $prepType = null, $prepVars = null){

    if(!$stmt = $this->con->prepare($sql)){
      die($this->con->error);
    }

    if($prepVars){
      if(!$stmt->bind_param($prepType, $prepVars)){
        die($this->con->error);
      }
    }

    if(!$stmt->execute()){
      echo $this->con->error;
    }

    $result = $stmt->get_result();
    if($result->num_rows == 0){
      return false;
    }

    while($row = $result->fetch_assoc()){
      $this->resultArray[] = $row;
    }

    return $this->resultArray;
  }

  public function deleteQuery($sql, $prepType, ...$prepVars){
    if(!$stmt = $this->con->prepare($sql)){
      die(var_dump($this->con->error_list));
    }

    if(is_array($prepVars)){
      if(!$stmt->bind_param($prepType, ...$prepVars)){
        die($stmt->error);
      }
    }else{
      if(!$stmt->bind_param($prepType, $prepVars)){
        die($stmt->error);
      }
    }
    $exec = $stmt->execute();
    if ( false === $exec ) {
      error_log('mysqli execute() failed: ');
      error_log( print_r( htmlspecialchars($stmt->error), true ) );
    }else{
      return true;
    }

  }

  public function selectQuery2($sql, $prepType, ...$prepVars){
    if(!$stmt = $this->con->prepare($sql)){
      die(var_dump($this->con->error_list));
    }
    if(!$stmt->bind_param($prepType, ...$prepVars)){
      die($stmt->error);
    }
    $stmt->execute();
    $result = $stmt->get_result();
    if($result->num_rows == 0){
      return false;
    }

    while($row = $result->fetch_assoc()){
      $this->resultArray[] = $row;
    }

    return $this->resultArray;
  }

  public function insertQuery($sql, $prepType, ...$prepVars){
    $stmt = $this->con->prepare($sql);
    $stmt->bind_param($prepType, ...$prepVars) || die($stmt->error);
    $stmt->execute();
    return $stmt->insert_id;
  }

}
class access {
  public $current_user;
  public $token;
  public $email_address;
  public $user_name;
  public $access_level = array();
  public $faggruppe;


  public function __construct(){
    if(isset($this->current_user)){
      $this->check_access_level();
    }
    if(!isset($_SESSION['termin'])) {
      $this->year = DEFAULT_YEAR;
      $this->termin = DEFAULT_TERM;


      $sql = "SELECT termin.id FROM termin WHERE termin.year = ? AND termin.month = ?";
      $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
      if (!$con->set_charset("utf8")) {
        printf("Error loading character set utf8: %s\n", $con->error);
      }
      $stmt = $con->prepare($sql);
      $stmt->bind_param("ss", $this->year, $this->termin);
      $stmt->execute();
      $stmt->bind_result($this->terminid);
      $stmt->fetch();
      $con->close();
      $_SESSION['termin'] = $this->terminid;
    } else {
      $this->terminid = $_SESSION['termin'];
    }
  }

  public function hent_terminer(){
    $sql = "SELECT * FROM termin";
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $result = $stmt->get_result();
    $terminer = '';
    while ($row = $result->fetch_assoc()){
      if($row['id'] == $_SESSION['termin']){
        $terminer = $terminer . "<option value='". $row['id'] ."' selected> " . $row['year'] . "-" . $row['month'] . "</option>\r\n";
      }else{
        $terminer = $terminer . "<option value='". $row['id'] ."'> " . $row['year'] . "-" . $row['month'] . "</option>\r\n";
      }
    }
    $con->close();
    return($terminer);
  }

  public function check_access_level(){
    $sql = "SELECT rolleid FROM tilganger WHERE brukerid = ?";
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    $stmt = $con->prepare($sql);
    $stmt->bind_param('i', $this->current_user);
    $stmt->execute();
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()){
      $this->access_level = $row['rolleid'];
    }
    $sql = "SELECT faggruppe FROM brukere WHERE id = ?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param('i', $this->current_user);
    $stmt->execute();
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()){
      $this->faggruppe = $row['faggruppe'];
    }
    $con->close();
  }

  public function logout(){
    $sql = "DELETE FROM tokens WHERE token = ?";
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    $stmt = $con->prepare($sql);
    $stmt->bind_param("s", $this->token);
    $stmt->execute();
    $con->close();
  }

  public function check_access_token(){
    $sql = "SELECT id, UNIX_TIMESTAMP(expires) FROM tokens WHERE token = ?";
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    $stmt = $con->prepare($sql);
    $stmt->bind_param("s", $this->token);
    $stmt->execute();
    $stmt->bind_result($this->current_user, $expires);
    $stmt->fetch();

    if ($expires < time()){
      return(false);
    } else {
      $sql = "SELECT navn, email FROM brukere WHERE id = ?";
      $stmt->close();
      $stmt = $con->prepare($sql);
      $stmt->bind_param("i", $this->current_user);
      $stmt->execute();
      $stmt->bind_result($this->user_name, $this->email_address);

      $stmt->fetch();
      if (DEBUG) {
        echo 'brukernavn:' . $this->user_name;
      }
      $stmt->close();
      $sql = "UPDATE tokens SET expires = FROM_UNIXTIME(?) WHERE token = ?";
      $stmt = $con->prepare($sql);
      $expires = time() + TOKEN_VALIDITY;
      $stmt->bind_param("is", $expires,$this->token);
      $stmt->execute();
      $con->close();
      return true;
    }
  }

  function create_token(
    int $length = 64,
    string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
    if ($length < 1) {
      throw new \RangeException("Length must be a positive integer");
    }
    $pieces = [];
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
      $pieces []= $keyspace[random_int(0, $max)];
    }
    $this->token = implode('', $pieces);
  }

  function email ($rcpt, $topic, $msg, $from){
    if(!mail($rcpt, $topic, $msg, 'from:' . $from)){
      echo "Mailsending fungerte ikke!";
      die();
    }
  }

  public function send_login(){
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    $sql = "SELECT id FROM brukere WHERE email = ?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param('s', $this->email_address);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows < 1){
      if(DEBUG){
        echo $stmt->error;
        //echo $this->email_address;
      }
      return(false);
    }else {
      $stmt->bind_result($this->current_user);
      $stmt->fetch();
      $stmt->close();

      $this->create_token();
      $topic = "OppgaveWeb login";
      $sender = "noreply@ibm.ntnu.no\r\n";
      $sender .= "MIME-Version: 1.0\r\n";
      $sender .= "Content-Type: text/html; charset=UTF-8\r\n";
      $body = '<html><body>English below <br />' .
        '<p>For å logge inn i OppgaveWeb, bruk <a href="' . SCRIPT_URL . '/login.php?token=' . $this->token . '">denne lenken</a><br />' .
        'Lenken er gyldig til ' . date('Y-m-d H:i:s', time() + TOKEN_VALIDITY) . '</p>' .
        '<p>To log on to the OppgaveWeb, use <a href="' . SCRIPT_URL . '/login.php?token=' . $this->token . '">this link</a><br />' .
        'The link is valid until ' . date('Y-m-d H:i:s', time() + TOKEN_VALIDITY) . '</p></body><html>';
      $this->email($this->email_address, $topic, $body, $sender);

      $sql = "INSERT INTO tokens (id, token, expires) VALUES (?, ?, FROM_UNIXTIME(?))";

      $stmt = $con->prepare($sql);
      $time = time() + TOKEN_VALIDITY;
      $stmt->bind_param('isi', $this->current_user, $this->token, $time);
      $stmt->execute();
      $stmt->close();
    }
  }
}

class studentoppgave {
  public $studentid;
  public $oppgaveid;
  public $veilederid;
  public $ekstern;
  public $intern;

  public function list_emne($year = DEFAULT_YEAR, $term = DEFAULT_TERM){

  }
}

class oppgave {
  public $year;
  public $termin;
  public $oppgaveliste = array();
  public $terminid;

  function __construct(){
    if(!isset($_SESSION['termin'])) {
      $this->year = DEFAULT_YEAR;
      $this->termin = DEFAULT_TERM;


      $sql = "SELECT termin.id FROM termin WHERE termin.year = ? AND termin.month = ?";
      $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
      if (!$con->set_charset("utf8")) {
        printf("Error loading character set utf8: %s\n", $con->error);
      }
      $stmt = $con->prepare($sql);
      $stmt->bind_param("ss", $this->year, $this->termin);
      $stmt->execute();
      $stmt->bind_result($this->terminid);
      $stmt->fetch();
      $con->close();
      $_SESSION['termin'] = $this->terminid;
    } else {
      $this->terminid = $_SESSION['termin'];
    }
  }

  public function list_oppgaver($faggruppe = 0){
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $sql = "SELECT vurderingsenhet.emne, vurderingsenhet.id as vurdid, brukere.id as brukerid, brukere.navn as emneansvarlig, faggrupper.id as fag_id, faggrupper.forkortelse as fag_kort, faggrupper.navn as fag_lang, emner.emnenavn as emnenavn FROM vurderingsenhet ";
    $sql .= "INNER JOIN emner ON vurderingsenhet.emne=emner.emnekode ";
    $sql .= "INNER JOIN brukere ON emner.emneansvarlig=brukere.id ";
    $sql .= "INNER JOIN faggrupper ON emner.faggruppe=faggrupper.id";
    if($faggruppe == 0) {
      $sql .= " WHERE vurderingsenhet.termin = ? ";
      $stmt = $con->prepare($sql);
      $stmt->bind_param("i", $this->terminid);
    }else{
      $sql .= " WHERE vurderingsenhet.termin = ? AND emner.faggruppe = ?";
      $stmt = $con->prepare($sql);
      $stmt->bind_param("ii", $this->terminid, $faggruppe);
    }
    //echo $sql;

    $stmt->execute();
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()){
      $this->oppgaveliste[] = $row;
    }
    $con->close();
    //var_dump($this->oppgaveliste);
  }
}

class vurderingsenhet{
  public $id;
  public $studentliste = array();
  public $brukerliste = array();
  public $emnekode;
  public $termin;
  public $faggruppe;
  public $ekstern_sensor;
  public $count = 0;
  public $count_locked = 0;
  public $count_veileder = 0;
  public $count_sensor1 = 0;
  public $count_sensor2 = 0;
  public $emnetype;
  public $emnetekst;

  public function idToName($id, $field){

    switch($field){
      case "veileder":
        foreach ($this->brukerliste as $bruker){
          if ($bruker['id'] == $id){
              return($bruker['navn'] .", ". $bruker['fornavn']);
            }
        }
        break;

      case "sensor":
        foreach ($this->ekstern_sensor as $bruker){
          if ($bruker['id'] == $id){
            return($bruker['navn'] .", ". $bruker['fornavn']);
          }
        }
        break;

      default:
        return ("");
    }
  }

  public function sett_emnetype(){
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $sql = "SELECT type FROM emner WHERE emnekode = ?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("s",$this->emnekode);
    $stmt->execute();
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()){
      $this->emnetype = $row['type'];
    }
    $sql = "SELECT description FROM emnetyper where id = ?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("i",$this->emnetype);
    $stmt->execute();
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()){
      $this->emnetekst = $row['description'];
    }
  }
  public function list_meldinger(){
    $this->list_brukere();
    $this->list_eksterne();
    $this->sett_emnetype();
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $sql = "SELECT vurderingsmelding.*, studenter.navn as navn, studenter.epost as epost FROM vurderingsmelding ";
    $sql .= "INNER JOIN studenter ON vurderingsmelding.studentid=studenter.id ";
    $sql .= "WHERE vurderingsmelding.vurderingsenhet = ? ORDER BY studenter.navn ASC";
    //echo $sql;
    $stmt = $con->prepare($sql);
    $stmt->bind_param('i', $this->id);
    $stmt->execute();
    $result = $stmt->get_result();
    $this->studentliste = array();
    while($row = $result->fetch_assoc()){
      $this->studentliste[] = $row;
    }
    if(DEBUG) {
      var_dump($this->studentliste);
    }
    $stmt->close();
  }

  public function count_locked(){
    $this->list_brukere();
    $this->list_eksterne();
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $sql = "SELECT vurderingsmelding.locked FROM vurderingsmelding ";
    $sql .= "WHERE vurderingsmelding.vurderingsenhet = ?";
    //echo $sql;
    $stmt = $con->prepare($sql);
    $stmt->bind_param('i', $this->id);
    $stmt->execute();
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()){
      if($row['locked']){
        $this->count_locked ++;
        $this->count ++;
      }else{
        $this->count ++;
      }
    }

    $sql = "SELECT count(studentid) FROM vurderingsmelding ";
    $sql .= "WHERE vurderingsmelding.vurderingsenhet = ? AND veileder IS NOT NULL";
    //echo $sql;
    $stmt = $con->prepare($sql);
    $stmt->bind_param('i', $this->id);
    $stmt->execute();
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()){
      $this->count_veileder = $row['count(studentid)'];
    }
    $sql = "SELECT count(studentid) FROM vurderingsmelding ";
    $sql .= "WHERE vurderingsmelding.vurderingsenhet = ? AND sensor1 IS NOT NULL";
    //echo $sql;
    $stmt = $con->prepare($sql);
    $stmt->bind_param('i', $this->id);
    $stmt->execute();
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()){
      $this->count_sensor1 = $row['count(studentid)'];
    }
    $sql = "SELECT count(studentid) FROM vurderingsmelding ";
    $sql .= "WHERE vurderingsmelding.vurderingsenhet = ? AND sensor2 IS NOT NULL";
    //echo $sql;
    $stmt = $con->prepare($sql);
    $stmt->bind_param('i', $this->id);
    $stmt->execute();
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()){
      $this->count_sensor2 = $row['count(studentid)'];
    }

    $stmt->close();
  }

  public function get_faggruppe(){
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    //var_dump($this->id);
    if(empty($this->emnekode) AND !empty($this->id)){
	    $sql = "SELECT emne FROM vurderingsenhet WHERE id = ?";
	    $stmt = $con->prepare($sql);
	    $stmt->bind_param("i", $this->id);
	    $stmt->execute();
	    $stmt->bind_result($this->emnekode);
	    $stmt->fetch();
	    //var_dump($this->emnekode);
	    $stmt->close();
    }
    $sql = "SELECT faggruppe FROM emner WHERE emnekode = ?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("s", $this->emnekode);
    $stmt->execute();
    $stmt->bind_result($this->faggruppe);
    $stmt->fetch();
    $stmt->close();
  }

  public function list_brukere(){
    //$this->get_faggruppe();
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $sql = "SELECT brukere.* FROM brukere ORDER BY navn ASC";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $result = $stmt->get_result();
    $this->brukerliste = array();
    while($row = $result->fetch_assoc()){
      $this->brukerliste[] = $row;
    }
    //var_dump($this->brukerliste);
    $stmt->close();
  }

  public function list_interne_brukere(){
    $this->get_faggruppe();
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $sql = "SELECT brukere.* FROM brukere WHERE brukere.faggruppe = ? ORDER BY navn ASC";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("i", $this->faggruppe);
    $stmt->execute();
    $result = $stmt->get_result();
    $this->brukerliste = array();
    while($row = $result->fetch_assoc()){
      $this->brukerliste[] = $row;
    }
    //var_dump($this->faggruppe);
    //var_dump($this->brukerliste);
    $stmt->close();
  }

  public function list_eksterne(){
    $this->get_faggruppe();
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $sql = "SELECT * FROM ekstern_sensor ORDER BY navn ASC";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $result = $stmt->get_result();
    $this->ekstern_sensor = array();
    while($row = $result->fetch_assoc()){
      $this->ekstern_sensor[] = $row;
    }
    //var_dump($this->brukerliste);
    $stmt->close();
  }

  public function lagre_endringer(){
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $keys = array_keys($_POST);
    if(DEBUG){
      var_dump($keys);
    }
    foreach ($keys as $row){
      $subkeys = explode('-', $row);
      //var_dump($subkeys);
      $sql = "UPDATE vurderingsmelding SET $subkeys[1] = ? WHERE studentid = ?";
      if(!$stmt = $con->prepare($sql)){
        echo $con->error;
      }
      $stmt->bind_param("ii", $_POST[$row], $subkeys[0]);
      //echo "UPDATE vurderingsmelding SET " . $subkeys[1] . " = " . $_POST[$row] . " WHERE studentid = ".$subkeys[0];
      $stmt->execute();
    }
  }
}

class emne{
  public $emnekode;
  public $vurderingsenheter = array();
  public $year = DEFAULT_YEAR;
  public $month = DEFAULT_TERM;
  public $terminid;

  public function finn_terminid(){
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    $sql = "SELECT *  FROM termin WHERE year = ? AND month = ? ORDER BY YEAR DESC";
    //echo("SELECT *  FROM termin WHERE year = ". $this->year ." AND month = ". $this->month ." ORDER BY YEAR DESC");
    $stmt = $con->prepare($sql);
    $stmt->bind_param('ss', $this->year, $this->month);
    $stmt->execute();
    $result = $stmt->get_result();
    $this->terminid = array();
    while($row = $result->fetch_assoc()){
      $this->terminid[] = $row;
    }
    //var_dump($this->terminid);
    $stmt->close();
    $con->close();
  }

  public function list_terminer(){
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    $sql = "SELECT vurderingsenhet.id, termin.id as terminid, termin.year as year, termin.month as month  FROM vurderingsenhet ";
    $sql .= "INNER JOIN termin ON vurderingsenhet.termin=termin.id ";
    $sql .= "WHERE vurderingsenhet.emne = ?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param('s', $this->emnekode);
    $stmt->execute();
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()){
      $this->vurderingsenheter[] = $row;
    }
    $stmt->close();
  }

  public function list_emner(){
    $this->list_terminer();
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    $sql = "SELECT vurderingsenhet.id as enhetsid, vurderingsenhet.emne as emne FROM vurderingsenhet WHERE termin = ?";
    //echo("SELECT vurderingsenhet.id as enhetsid, vurderingsenhet.emne as emne FROM vurderingsenhet WHERE termin = ". $this->terminid[0]['id']);
    $stmt = $con->prepare($sql);
    $stmt->bind_param('i', $this->terminid[0]['id']);
    $stmt->execute();
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()){
      //var_dump($row);
      $this->vurderingsenheter[] = $row;
    }
    //var_dump($this->vurderingsenheter);
    $stmt->close();
  }


}

class student extends rapporter {
  public $studentid;
  public $studentinfo = array();
  public $vurderingsenhet = null;

  public function listStudMeldinger($vurdid = false){
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $sql = "SELECT vurderingsmelding.*, studenter.navn as navn, studenter.epost as epost, ";
    $sql .= "termin.month as month, termin.year as year, ";
    $sql .= "vurderingsenhet.termin as termin, vurderingsenhet.emne as emne, vurderingsenhet.id as vurdid FROM vurderingsmelding ";
    $sql .= "INNER JOIN studenter ON vurderingsmelding.studentid=studenter.id ";
    $sql .= "INNER JOIN vurderingsenhet ON vurderingsmelding.vurderingsenhet=vurderingsenhet.id ";
    $sql .= "INNER JOIN termin ON vurderingsenhet.termin=termin.id ";
    $sql .= "WHERE vurderingsmelding.studentid = ?";
    if(!$vurdid) {
      $stmt = $con->prepare($sql);
      $stmt->bind_param("i", $this->studentid);
    }else{
      $sql .= " AND vurderingsmelding.id = ?";
      $stmt = $con->prepare($sql);
      $stmt->bind_param("ii", $this->studentid, $vurdid);
    }
    $stmt->execute();
    $result = $stmt->get_result();
    if($result){
      while($row = $result->fetch_assoc()){
        $this->studentinfo[] = $row;
      }
    }else{
      echo $stmt->error;
    }
  }


}

class brukere{
  public $brukerliste = array();

  public function finn_brukere($rolle = 2){
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $sql = "SELECT brukere.*  FROM brukere INNER JOIN tilganger ON brukere.id=tilganger.brukerid WHERE tilganger.rolleid = ? ORDER BY navn ASC";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("i", $rolle);
    $stmt->execute();
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()){
      //var_dump($row);
      $this->brukerliste[] = $row;
    }
    //var_dump($this->vurderingsenheter);
    $stmt->close();
  }
}

class log{
  public $sensor;
  public $student;
  public $endring;

  public function log_student($bruker){

  }

  public function log_sensor($bruker){
    $sql = "INSERT INTO log_sensor (id, changed_by, logg) VALUES (?, ?, ?)";
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $stmt = $con->prepare($sql);
    $stmt->bind_param("iis", $this->sensor, $bruker, $this->endring);
    $stmt->execute();
  }

  public function log_bruker($bruker){

  }

  public function log_emne($bruker){

  }
}
class rapporter extends vurderingsenhet {
  public $studentliste;
  public $emneliste;
  public $terminliste;
  public $brukerlisteSorted;
  public $ekstern_sensorSorted;

  function __construct(){
    $this->emneliste = array();
    $this->terminliste = array();
    $this->list_brukere();
    $this->list_eksterne();
    $this->sortBrukerliste();
    $this->sortEksternSensor();
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $sql = "SELECT * from vurderingsenhet";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $result = $stmt->get_result();
    if($result){
      while($row = $result->fetch_assoc()){
        $this->emneliste[] = $row;
      }
    }else{
      echo $stmt->error;
    }
    $sql = "SELECT * from termin";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $result = $stmt->get_result();
    if($result){
      while($row = $result->fetch_assoc()){
        $this->terminliste[] = $row;
      }
    }else{
      echo $stmt->error;
    }
  }

  function sortBrukerliste(){
    foreach ($this->brukerliste as $bruker){
      $this->brukerlisteSorted[$bruker['id']] = $bruker;
    }
  }

  function sortEksternSensor(){
    foreach ($this->ekstern_sensor as $bruker){
      $this->ekstern_sensorSorted[$bruker['id']] = $bruker;
    }
  }

  function veilederoppgaver($veileder, $termin, $limit = false){
    $this->studentliste = null;
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $sql = "SELECT vurderingsmelding.*, studenter.navn as navn, studenter.epost as epost, ";
    $sql .= "termin.month as month, termin.year as year, ";
    $sql .= "vurderingsenhet.termin as termin, vurderingsenhet.emne as emne, vurderingsenhet.id as vurdid FROM vurderingsmelding ";
    $sql .= "INNER JOIN studenter ON vurderingsmelding.studentid=studenter.id ";
    $sql .= "INNER JOIN vurderingsenhet ON vurderingsmelding.vurderingsenhet=vurderingsenhet.id ";
    $sql .= "INNER JOIN termin ON vurderingsenhet.termin=termin.id ";
    if(!$limit) {
      $sql .= "WHERE veileder = ?";
      /* echo($sql); */
      $stmt = $con->prepare($sql);
      $stmt->bind_param("i", $veileder);
    }else{
      $sql .= "WHERE veileder = ? AND vurderingsenhet.termin = ?";
      if(!$stmt = $con->prepare($sql)){
        echo $con->error;
      }
      if(!$stmt->bind_param("ii", $veileder, $termin)){
        echo($stmt->error);
      }
    }
    $stmt->execute();
    if($result = $stmt->get_result()){
      while($row = $result->fetch_assoc()){
        $this->studentliste['studentliste'][] = $row;
      }
    }else{
      echo $stmt->error;
    }
    if($this->studentliste != null) {
      $i = 0;
      foreach ($this->studentliste['studentliste'] as $row) {
        if ($row['toeksterne'] == 0) {
          if ($row['sensor1'] != null) {
            $row['sensor1'] = $this->brukerlisteSorted[$row['sensor1']]['navn'] . ", " . $this->brukerlisteSorted[$row['sensor1']]['fornavn'];
          } else {
            $row['sensor1'] = "";
          }
          if ($row['sensor2'] != null) {
            $row['sensor2'] = $this->ekstern_sensorSorted[$row['sensor2']]['navn'] . ", " . $this->ekstern_sensorSorted[$row['sensor1']]['fornavn'];
          } else {
            $row['sensor2'] = "";
          }
        } else {
          if ($row['sensor1'] != null) {
            $row['sensor1'] = $this->ekstern_sensorSorted[$row['sensor1']]['navn'] . ", " . $this->ekstern_sensorSorted[$row['sensor1']]['fornavn'];
          } else {
            $row['sensor1'] = "";
          }
          if ($row['sensor2'] != null) {
            $row['sensor2'] = $this->ekstern_sensorSorted[$row['sensor2']]['navn'] . ", " . $this->ekstern_sensorSorted[$row['sensor1']]['fornavn'];
          } else {
            $row['sensor2'] = "";
          }
        }
        $row['veileder'] = $this->brukerlisteSorted[$row['veileder']]['navn'] . ", " . $this->brukerlisteSorted[$row['veileder']]['fornavn'];
        $this->studentliste['studentliste'][$i] = $row;
        $i++;
      }
      echo(json_encode(array_values($this->studentliste)));
    }else{
      echo(json_encode(array()));
    }
  }


  function sensoroppgaver($veileder, $termin, $limit = false)
  {
    $this->studentliste = null;
    $con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$con->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $con->error);
    }
    $sql = "SELECT vurderingsmelding.*, studenter.navn as navn, studenter.epost as epost, ";
    $sql .= "termin.month as month, termin.year as year, ";
    $sql .= "vurderingsenhet.termin as termin, vurderingsenhet.emne as emne, vurderingsenhet.id as vurdid FROM vurderingsmelding ";
    $sql .= "INNER JOIN studenter ON vurderingsmelding.studentid=studenter.id ";
    $sql .= "INNER JOIN vurderingsenhet ON vurderingsmelding.vurderingsenhet=vurderingsenhet.id ";
    $sql .= "INNER JOIN termin ON vurderingsenhet.termin=termin.id ";
    if (!$limit) {
      $sql .= "WHERE sensor1 = ? OR sensor2 = ?";
      /* echo($sql); */
      $stmt = $con->prepare($sql);
      $stmt->bind_param("ii", $veileder, $veileder);
    } else {
      $sql .= "WHERE sensor1 = ? OR sensor2 = ? AND vurderingsenhet.termin = ?";
      if (!$stmt = $con->prepare($sql)) {
        echo $con->error;
      }
      if (!$stmt->bind_param("iii", $veileder, $veileder, $termin)) {
        echo($stmt->error);
      }
    }
    $stmt->execute();
    if ($result = $stmt->get_result()) {
      while ($row = $result->fetch_assoc()) {
        $this->studentliste['studentliste'][] = $row;
      }
    } else {
      echo $stmt->error;
    }
    if ($this->studentliste != null) {
      $i = 0;
      foreach ($this->studentliste['studentliste'] as $row) {
        if ($row['toeksterne'] == 0) {
          if ($row['sensor1'] != null) {
            $row['sensor1'] = $this->brukerlisteSorted[$row['sensor1']]['navn'] . ", " . $this->brukerlisteSorted[$row['sensor1']]['fornavn'];
          } else {
            $row['sensor1'] = "";
          }
          if ($row['sensor2'] != null) {
            $row['sensor2'] = $this->ekstern_sensorSorted[$row['sensor2']]['navn'] . ", " . $this->ekstern_sensorSorted[$row['sensor2']]['fornavn'];
          } else {
            $row['sensor2'] = "";
          }
        } else {
          if ($row['sensor1'] != null) {
            $row['sensor1'] = $this->ekstern_sensorSorted[$row['sensor1']]['navn'] . ", " . $this->ekstern_sensorSorted[$row['sensor1']]['fornavn'];
          } else {
            $row['sensor1'] = "";
          }
          if ($row['sensor2'] != null) {
            $row['sensor2'] = $this->ekstern_sensorSorted[$row['sensor2']]['navn'] . ", " . $this->ekstern_sensorSorted[$row['sensor1']]['fornavn'];
          } else {
            $row['sensor2'] = "";
          }
        }
        if ($row['veileder'] != null) {
          $row['veileder'] = $this->brukerlisteSorted[$row['veileder']]['navn'] . ", " . $this->brukerlisteSorted[$row['veileder']]['fornavn'];
        }

        $this->studentliste['studentliste'][$i] = $row;
        $i++;
      }
      echo(json_encode(array_values($this->studentliste)));
    }else{
      echo(json_encode(array()));
    }
  }
}
class klagesensur extends sql{
  function setEmneansvarlig($emnekode, $emneansvarligEpost){
    //var_dump($emneansvarligEpost);
    $sql = "SELECT id, faggruppe FROM brukere WHERE email = ?";
    $result = $this->selectQuery($sql, 's', $emneansvarligEpost);

    $faggruppe = $result[0]['faggruppe'];
    if(!$result){
      $sql = "INSERT INTO brukere (email, faggruppe) values (?, ?)";
      $result = $this->insertQuery($sql, 'si', $emneansvarligEpost, 98);
      $faggruppe = 98;
    }
    $sql = "UPDATE emner SET emneansvarlig = ?, faggruppe = ? WHERE emnekode = ?";
    $id = $result[0]['id'];
    $this->insertQuery($sql, 'iis', $id, $faggruppe, $emnekode);
    return $result;
  }
  function getAllCourses($type = 4){
    if($type == 4) {
      $sql = "SELECT * FROM emner WHERE type = $type";
    }else{
      $sql = "SELECT * FROM emner";
    }
    $result = $this->selectQuery($sql);
    return $result;
  }
  function getAllFaggruppe(){
    $sql = "SELECT * FROM faggrupper WHERE active = 1";
    $result = $this->selectQuery($sql);
    return $result;
  }
  function getAllEmneansvarlig(){
    $sql = "SELECT * FROM brukere WHERE active = 1 ORDER BY navn";
    $result = $this->selectQuery($sql);
    return $result;
  }
  function getEkstern($emnekode){
    $sql = "select * from `klagesensur-ekstern` where emne = ?";
    $result = $this->selectQuery($sql, 's', $emnekode);
    if(!$result){
      return (new stdClass);
    }else{
      return ($result);
    }
  }
  function getIntern($emnekode){
    $sql = "select * from `klagesensur-intern` where emne = ?";
    $result = $this->selectQuery($sql, 's', $emnekode);
    if(!$result){
//      return [(new stdClass)];
      return [];
    }else{
      return ($result);
    }
  }
  function getAlleEksternSensor(){
    $sql = "SELECT * FROM ekstern_sensor ORDER BY navn";
    return $this->selectQuery($sql);
  }
  function settEksternSensor($sensor, $emnekode){
    $sql = "INSERT INTO `klagesensur-ekstern` (emne, sensor) VALUES (?, ?)";
    $result = $this->insertQuery($sql, 'si', $emnekode, $sensor);
    return $result;
  }
  function settInternSensor($sensor, $emnekode){
    $sql = "INSERT INTO `klagesensur-intern` (emne, sensor) VALUES (?, ?)";
    var_dump($sensor);
    $result = $this->insertQuery($sql, 'si', $emnekode, $sensor);
    return $result;
  }
  function slettSensor($sensor, $user, $emnekode, $intext){
    $sqlInsert = "INSERT INTO slettedeSensorer (emnekode, sensor, user, intext) VALUES (?,?,?,?)";
    switch ($intext){
      case 'int':
        $sqlDelete = "DELETE FROM `klagesensur-intern` WHERE emne = ? AND sensor = ?";
        break;
      case 'ext':
        $sqlDelete = "DELETE FROM `klagesensur-ekstern` WHERE emne = ? AND sensor = ?";
        break;
      default:
        die('Missing mandatory argument intext');
    }
    $result = $this->insertQuery($sqlInsert, 'siis', $emnekode, $sensor, $user, $intext);
    var_dump($result);
    if($result){
      $result = $this->selectQuery2($sqlDelete, 'si', $emnekode, $sensor);
      return $result;
    }else{
      return $this->con->error;
    }
  }
  function getEmnetyper(){
    $sql = "SELECT * FROM emnetyper";
    $result = $this->selectQuery($sql);
    return $result;
  }
}
class emneadministrasjon extends klagesensur {
  function faggrupper($faggrupper){
    $newarray = [];
    foreach ($faggrupper as $faggruppe){
      $newrow = [];
      $newrow['forkortelse'] = $faggruppe['forkortelse'];
      $newrow['navn'] = $faggruppe['navn'];
      $newarray[$faggruppe['id']] = $newrow;
    }
    return $newarray;
  }

  function emnetyper($emner){
    $newarray = [];
    foreach ($emner as $emne){
      $newrow = [];
      $newrow['type'] = $emne['type'];
      $newrow['description'] = $emne['description'];
      $newarray[$emne['id']] = $newrow;
    }
    return $newarray;
  }

  function emneansvarlige($fagpersoner){
    $newarray = [];
    foreach ($fagpersoner as $fagperson){
      $newrow = [];
      $newrow['fornavn'] = $fagperson['fornavn'];
      $newrow['navn'] = $fagperson['navn'];
      $newrow['faggruppe'] = $fagperson['faggruppe'];
      $newarray[$fagperson['id']] = $newrow;
    }
    return $newarray;
  }

  function nyttEmne($array){
    //Lagre nytt emne i databasen
    //var_dump($array);
    $sql = "INSERT INTO emner (emnekode, emneansvarlig, faggruppe, emnenavn, type) VALUES (?, ?, ?, ?, ?)";
    $result = $this->insertQuery($sql, 'siisi', $array['emnekode'], $array['emneansvarlig'], $array['faggruppe'], $array['emnenavn'], intval($array['emnetype']));
    if($result == 0){
      return $array['emnekode'];
    }else{
      return $result;
    }

  }

  function lagreEmne($array){
    //Lagre nytt emne i databasen
    //var_dump($array);
    $sql = "UPDATE emner SET emneansvarlig = ?, faggruppe = ?, emnenavn = ?, type = ? WHERE emnekode = ?";
    $result = $this->insertQuery($sql, 'iisis', $array['emneansvarlig'], $array['faggruppe'], $array['emnenavn'], intval($array['emnetype']), $array['emnekode']);
    if($result == 0){
      return $array['emnekode'];
    }else{
      return $result;
    }

  }
  function slettEmne($emnekode){
    $sql = "DELETE FROM emner WHERE emnekode = ?";
    return $this->selectQuery($sql, 's', $emnekode);
  }
}
class fagpersonadmin extends sql{
  public function getAllRoller(){
    $sql = "SELECT * FROM roller";
    return $this->selectQuery($sql);
  }
  public function getAllFaggruppe(){
    $sql = "SELECT * FROM faggrupper";
    $result = $this->selectQuery($sql);
    return $result;
  }
  public function getAllEmneansvarlig(){
    $sql = "SELECT brukere.*,  tilganger.rolleid as tilgang FROM brukere ";
    $sql .= "INNER JOIN tilganger ON brukere.id=tilganger.brukerid ";
    $sql .= "ORDER BY navn";
    return $this->selectQuery($sql);
  }
  public function newFagperson($ar){
    $sql = "INSERT INTO brukere (email, navn, fornavn, faggruppe, active) VALUES (?,?,?,?,?)";
    if($ar['aktiv'] == 'true'){
      $ar['aktiv'] = true;
    }else{
      $ar['aktiv'] = false;
    }
    $result = $this->insertQuery($sql, 'sssii', $ar['epost'], $ar['etternavn'], $ar['fornavn'], $ar['faggruppe'], intval($ar['aktiv']));
    $sql2 = "INSERT INTO tilganger (brukerid, rolleid) VALUES (?,?)";
    $this->insertQuery($sql2, 'ii', $result, $ar['tilgang']);
    return $result;
  }
  public function saveFagperson($ar){
    if($ar['aktiv'] == 'true'){
      $ar['aktiv'] = true;
    }else{
      $ar['aktiv'] = false;
    }
    $sql = "UPDATE brukere SET email = ?, navn = ?, fornavn = ?, faggruppe = ?, active = ? WHERE id = ?";
    $result = $this->insertQuery($sql, 'sssiii', $ar['epost'], $ar['fornavn'], $ar['etternavn'], $ar['faggruppe'], intval($ar['aktiv']), $ar['id']);
    $sql2 = "UPDATE tilganger SET rolleid=? WHERE brukerid = ?";
    $this->insertQuery($sql2, 'ii', $ar['tilgang'], $ar['id']);
    if($result == 0){
      return $ar['id'];
    }else{
      return $result;
    }
  }
  public function deleteFagperson($id){
    $sql = "DELETE FROM tilganger WHERE brukerid = ?";
    $this->deleteQuery($sql, 'i', $id);
    $sql = "DELETE FROM brukere WHERE id = ?";
    $this->deleteQuery($sql, 'i', $id);
//    var_dump($result, $result2);
    return $id;
  }
}
