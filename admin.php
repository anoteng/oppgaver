<?php
include("config.php");
include("classes.php");
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION['access'])){
  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_level();
if($access->access_level != 1 AND $access->access_level != 2){
  header("Location: index.php");
}?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


  <script src="js/main.js"></script>
  <link rel="stylesheet" href="css/main.css">
  <style>
    img {
      max-width: 100%;
      height: auto;
    }
  </style>

</head>
<body id="body">

<br><br>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-2">
      <div class="panel-group" id="accordion1">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <a class="collapsed pull-right" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne">
              <div class="accordion-icon"></div>
            </a>
            <h4 class="panel-title" id="alle_emner">Vis alle emner</h4>
          </div>
          <!--<div id="collapseOne" class="panel-collapse collapse">

            <ul class="list-group">
              <li class="list-group-item">
                <div id="alle">alle oppgaver</div>
              </li>
              <li class="list-group-item">
                <div id="ledige">ledige oppgaver</div>
              </li>
            </ul>
          </div>-->
        </div>
      </div>
      <div class="panel-group" id="accordion2">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <a class="collapsed pull-right" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
              <div class="accordion-icon"></div>
            </a>
            <h4 class="panel-title" id="ny_oppgave">Admin</h4>
          </div>
          <div id="collapseTwo" class="panel-collapse collapse">
            <ul class="list-group">
              <li class="list-group-item">
                <div id="alle">Vurderingsenheter</div>
              </li>
              <li class="list-group-item">
                <div id="ledige">Studenter</div>
              </li>
              <li class="list-group-item">
                <div id="ledige">Faglærere</div>
              </li>
              <li class="list-group-item">
                <div id="ledige">Sensorer</div>
              </li>
              <li class="list-group-item">
                <div id="ledige">Tilganger</div>
              </li>
            </ul>
          </div>

          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-8" id="ajax-content">

    </div>
  </div>
</div>
<script>
  $('#ny_oppgave').on('click', function() {
    $('#ajax-content').load('https://org.ntnu.no/ibm/oppgaver/oppgave_ajax.php?oppgaver=ny');
  })
  $('#alle').on('click', function() {
    $('#ajax-content').load('https://org.ntnu.no/ibm/oppgaver/oppgave_ajax.php?oppgaver=alle');
  })

</script>
</body>
</html>
