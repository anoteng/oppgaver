<?php
include('config.php');
include('classes.php');
?>
<div class="container-fluid">
  <div class="filters">
    <div class="row">
      <h3>Fagpersonadmin (under utvikling)</h3>
    </div>
    <div class="row">
      <div class="col"><label>Emneansvarlig: <br><select class="js-example-basic-single" name="emneansvarlig" id="filterEmneansvarlig">
            <option value="null" selected>-</option>
            <?php
            $fagpersonadm = new fagpersonadmin();
            $rolleadm = new fagpersonadmin();
            $faggruppeadm = new fagpersonadmin();
            $fagpersoner = $fagpersonadm->getAllEmneansvarlig();
            $faggrupper = $faggruppeadm->getAllFaggruppe();
            $roller = $rolleadm->getAllRoller();
            foreach ($fagpersoner as $row){
              echo "<option value='$row[id]'>$row[navn], $row[fornavn]</option>";
            }
            ?>
          </select></label></div>

      <div class="col"><label>Faggruppe: <br><select name="faggruppe" id="filterFaggruppe">
            <option value="null" selected>-</option>
            <?php
            $sensor = new fagpersonadmin();
            $result = $sensor->getAllFaggruppe();
            foreach ($result as $row){
              echo "<option value='$row[id]'>$row[forkortelse] - $row[navn]</option>";
            }
            ?>
          </select></label></div>
      </label>
      <div class="col"></div>
    </div>
  </div>
  <div class="filters">
    <h3>Eksisterende fagpersoner</h3>
    <div id="emneHeading" class="row heading">
      <div class="col">Etternavn</div>
      <div class="col">Fornavn</div>
      <div class="col">Epost</div>
      <div class="col">Faggruppe</div>
      <div class="col">Tilgang</div>
      <div class="col">Aktiv</div>
      <div class="col"></div>
    </div>
    <div id="fagpersonNy" class="row">
      <div class="col"><input type="text" id="nyEtternavn" size="20"></div>
      <div class="col"><input type="text" id="nyFornavn" size="20"></div>
      <div class="col"><input type="text" id="nyEpost" size="20"></div>
      <div class="col"><select id="nyFaggruppe">
          <option selected>-</option>
          <?php
          foreach($faggrupper as $faggruppe){
            if($faggruppe['active']){
              echo "<option value='$faggruppe[id]'>$faggruppe[forkortelse]</option>";
            }
          }
          ?>
        </select></div>
      <div class="col"><select id="nyTilgang">
          <option selected>-</option>
        <?php
        foreach($roller as $rolle){
          echo "<option value='$rolle[id]'>$rolle[beskrivelse]</option>";
        }?>
        </select>
      </div>
      <div class="col"><input type="checkbox" id="nyAktiv" size="5" checked></div>
      <div class="col">
        <button class='btn' id='nyBtnSave' title='Lagre ny fagperson'>
          <img src='img/save.png' style='width: 20px'>
        </button>
      </div>
    </div>
    <div id="fagpersonTemplate" class="row hidden">
      <div class="col"><input type="text" id="templateEtternavn" size="20"></div>
      <div class="col"><input type="text" id="templateFornavn" size="20"></div>
      <div class="col"><input type="email" id="templateEpost" size="20"></div>
      <div class="col">
        <select id="templateFaggruppe" disabled>
          <?php
          foreach($faggrupper as $faggruppe){
            if($faggruppe['active']){
              echo "<option value='$faggruppe[id]'>$faggruppe[forkortelse]</option>";
            }
          }
          ?>
        </select>
      </div>
      <div class="col">
        <select id="templateTilgang" disabled>
          <?php
          foreach($roller as $rolle){
            echo "<option value='$rolle[id]'>$rolle[beskrivelse]</option>";
          }
          ?>
        </select>
      </div>
      <div class="col"><input type="checkbox" id="templateAktiv" size="5"></div>
      <div class="col">
        <button class='btn' id='btnEdit-template' title='Rediger fagperson'>
          <img src='img/edit.png' style='width: 20px'>
        </button>
        <button class='btn hidden' id='btnSave-template' title='Lagre endringer'>
          <img src='img/save.png' style='width: 20px'>
        </button>
        <button class='btn' id='btnDelete-template' title='Slett fagperson'>
          <img src='img/delete.png' style='width: 20px'>
        </button>
        <button class='btn' id='btnTransfer-template' title='overfør fagperson til ekstern sensor'>
          <img src='img/fagperson.png' style='width: 20px'>
        </button>
      </div>
    </div>
    <?php


    ?>
    <div id="fagpersonliste">
    <?php

    foreach($fagpersoner as $fagperson) {
      echo "
    <div id='row-$fagperson[id]' class='row rowhover'>
      <div class='col'><input type='text' id='etternavn-$fagperson[id]' size='20' value='$fagperson[navn]'></div>
      <div class='col'><input type='text' id='fornavn-$fagperson[id]' size='20' value='$fagperson[fornavn]'></div>
      <div class='col'><input type='text' id='epost-$fagperson[id]' size='20' value='$fagperson[email]'></div>
      <div class='col'><select disabled id='faggruppe-$fagperson[id]'>";
      foreach($faggrupper as $faggruppe){

        if($faggruppe['id']== $fagperson['faggruppe']){
          echo "<option value='$faggruppe[id]' selected>$faggruppe[forkortelse]</option>";
        }else{
          if($faggruppe['active']){
            echo "<option value='$faggruppe[id]'>$faggruppe[forkortelse]</option>";
          }
        }
      }
      echo "</select></div>";
      echo "<div class='col'><select disabled id='tilgang-$fagperson[id]'>";
      foreach($roller as $rolle){
        if($rolle['id'] == $fagperson['tilgang']){
          echo "<option value='$rolle[id]' selected>$rolle[beskrivelse]</option>";
        }else{
          echo "<option value='$rolle[id]'>$rolle[beskrivelse]</option>";
        }
      }
      echo "</select></div>";
      if($fagperson['active'] == 1){
        echo "<div class='col'><input type='checkbox' id='aktiv-$fagperson[id]' size='5' checked disabled></div>";
      }else{
        echo "<div class='col'><input type='checkbox' id='aktiv-$fagperson[id]' size='5' disabled></div>";
      }
      echo "
      <div class='col'>
        <button class='btn' id='btnEdit-$fagperson[id]' title='Rediger fagperson'>
            <img src='img/edit.png' style='width: 20px'>
        </button>
        <button class='btn hidden' id='btnSave-$fagperson[id]' title='Lagre endringer'>
            <img src='img/save.png' style='width: 20px'>
        </button>
        <button class='btn' id='btnDelete-$fagperson[id]' title='Slett fagperson'>
            <img src='img/delete.png' style='width: 20px'>
        </button>
        <button class='btn' id='btnTransfer-$fagperson[id]' title='overfør fagperson til ekstern sensor'>
            <img src='img/fagperson.png' style='width: 20px'>
        </button>
      </div>
    </div>";
    }
    ?>
    </div>
  </div>
</div>
