<?php
require_once('config.php');
require_once('classes.php');
if(!isset($_SESSION['access'])){
  header("Location: login.php");
}
$access = $_SESSION['access'];
$access->check_access_token();
$access->check_access_level();
?>
<script src="js/papaparse.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>
<script>

  function parseCSV(){
    var input = document.getElementById("dealCsv");
    var data = input.files[0];
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById('resultat').innerText += this.responseText.concat("<br />\r\n");
      }
    };
    Papa.parse(data, {
      header: true,
      complete: function(results, file) {
          httpRequest.open('POST', "import_ajax.php", true);
          httpRequest.setRequestHeader("Content-Type", "application/json");
          var jsonData = JSON.stringify(results.data);
          httpRequest.send(jsonData);
        }
      });
      //console.log(b_data);

      // httpRequest.open('POST', "import_ajax.php", true);
      // httpRequest.setRequestHeader("Content-Type", "application/json");
      // var jsonData = JSON.stringify(b_data);
      // httpRequest.send(jsonData);
  };

function treatCsv(data){
	var keys = {};
  var a_data = {};
  data[0].forEach(function(val, i){
  keys[i] = val;
  });
  data.forEach(function(val, i){
    if (i != 0){
      val[i].forEach(function(val2, j){
        a_data[keys[j]] = val2;

      });

    }
  });
  console.log(a_data);
  };
</script>
<P>Last opp kommaseparert CSV-fil med tegnkoding UTF-8</P>
<P>Fila må minimum inneholde kolonnene "emnekode", "arstall", "vurdtidkode", "studentnr_tildelt", "etternavn" og "fornavn". Dersom fila inneholder "emailadresse" blir også denne importert.</P>
<p>Hent ut vurderingsmeldinger fra FS:<br>
<ol>
  <li>Åpne rapport FS521.001</li>
  <li>Kjør utplukk på sted (eller emne om du du kun vil legge inn et enkelt emne)</li>
  <li>Legg inn ønsket stedkode (eller vurderingsenhet ved enkeltemne) og termin (VÅR/SOM/HØST)</li>
  <li>Legg inn filter for å bare få inn oppgaveemner ved å høyreklikke i vinduet of velge filtrer</li>
  <li>Eksempel på filter som henter alle prosjekt- og masteroppgaver undet TBA-prefikset:<br>
    ((emnekode LIKE 'TBA49%') OR (emnekode LIKE 'TBA45%')) AND (emnekode NOT LIKE 'TBA4565')<br>
    Leddet "AND (emnekode NOT LIKE 'TBA4565)" er med for å luke ut et enkeltemne med TBA45-prefix som ikke er fordypningsprosjekt
  </li>
  <li>Kjør rapporten (hurtigtast CTRL+r), dette vil ta litt tid ved rapport på stedkode</li>
  <li>Lagre rapporten som CSV-fil: høyreklikk, lagre som, kommaseparert fil.<br>
  Lagre fila et sted du finner den igjen fra lokal PC og last den opp under</li>
</ol>
  Systemet vil opprette studenter, terminer og vurderingsmeldinger ved behov, eksisterende oppføringer blir ikke overskrevet eller duplisert. Emnekoden må finnes i databasen fra før av.</p>
<p>
  <input type="file" id="dealCsv"/><br />
  <button type="submit" id="submitbutton" onclick="parseCSV()">Importer CSV</button>
</p>
<div id="resultat"></div>
<!--<script>
  function uploadDealcsv () {};

  /*------ Method for read uploded csv file ------*/
  uploadDealcsv.prototype.getCsv = function(e) {

    let input = document.getElementById('dealCsv');
    input.addEventListener('change', function() {

      if (this.files && this.files[0]) {

        var myFile = this.files[0];
        var reader = new FileReader();

        reader.addEventListener('load', function (e) {

          let csvdata = e.target.result;
          parseCsv.getParsecsvdata(csvdata); // calling function for parse csv data
        });

        reader.readAsText(myFile);
      }
    });
  }

  /*------- Method for parse csv data and display --------------*/
  uploadDealcsv.prototype.getParsecsvdata = function(data) {

    let parsedata = [];

    let newLinebrk = data.split("\n");
    for(let i = 0; i < newLinebrk.length; i++) {

      parsedata.push(newLinebrk[i].split(","))
    }

    console.table(parsedata);
  }



  var parseCsv = new uploadDealcsv();
  parseCsv.getCsv();
</script>-->
